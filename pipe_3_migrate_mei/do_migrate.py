#!/usr/bin/env python3
import pprint
import time
import subprocess
import sys
import os
import logging
import psycopg2
import yaml


def setup_logging(log_name, log_level):
    """
    Set up the logger
    :param log_name: the name of logger
    :param log_level: the level
    :return:
    """
    log_level = log_level.upper()
    log = logging.getLogger(log_name)
    log_format = "%(asctime)s - (%(levelname)s) - %(name)s - %(message)s"
    logging.basicConfig(level=log_level,
                        format=log_format,
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.StreamHandler(sys.__stdout__)
    logging.getLogger().setLevel(log_level)
    return log


def pg_connect(db):
    pg = psycopg2.connect(database=db['database']
                          , user=db['username']
                          , port=db['port']
                          , host=db['hostname']
                          , password=db['password'])
    pg.autocommit = True
    return pg


def call_converter(converter, conf, log, debug=False):
    cmd = [converter, '-f', conf]
    if debug:
        cmd.append('-v')
        log.info(cmd)
    subprocess.Popen(cmd).communicate()


def create_dummy(pg):
    sql = "CREATE table tt2 (id int);"
    cur = pg.cursor()
    cur.execute(sql)
    cur.close()


def set_search_path(pg, schema_out, log):
    """
    Set default search path of user to target schema.
    This way tables are copied to the target schema instead of public.
    :param pg: the connection
    :param schema_out: the name of the target schema
    :param log: the logger
    :return:
    """
    log.info(' Set search path to ' + schema_out)
    sql = """ALTER USER etl SET search_path = {schema};""".format(schema=schema_out)
    _execute(pg, sql)


def grant_usage_target_schema(pg, schema, log):
    """
    Grant usage an select on target schema to root.
    :param pg: the connection
    :param schema: the name of the target schema
    :param log: the logger
    :return:
    """
    log.info(' Grant rights to root on schema: ' + schema)
    sql = """GRANT USAGE ON SCHEMA {schema} TO root;
        GRANT ALL PRIVILEGES ON SCHEMA {schema} TO root;
        GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA {schema} TO root;
        """.format(schema=schema)
    _execute(pg, sql)


def _execute(pg, sql):
    cur = pg.cursor()
    cur.execute(sql)
    cur.close()


def empty_schema(pg, schema, log):
    log.debug(' Empty schema ' + schema)
    cur = pg.cursor()
    tables = get_table_names(pg, schema)
    sql = """"""
    for table in tables:
        sql += """
         DROP TABLE {schema}.{table};
         """.format(schema=schema, table=table)
    log.debug(sql)
    if tables:
        cur.execute(sql)
    cur.close()


def select_list(pg, sql):
    cur = pg.cursor()
    cur.execute(sql)
    l = list()
    for r in cur.fetchall():
        l.append(r[0])
    cur.close()
    return l


def get_table_names(pg, schema):
    sql = """
        select tablename
        from pg_tables
        where schemaname = '{schema}'
        """.format(schema=schema)
    return select_list(pg, sql)


################################################################################################


def do_migrate(config_file, schema):
    """
    - open conf file
    - set search path to target schema
    - empty target schema
    - move tables
    - grant right on target schema to root.

    :param config_file:
    :param schema:
    :return:
    """
    # Open config
    conf = yaml.load(open(config_file, "r"))[schema]
    conf_path = os.path.dirname(config_file)
    log_level = conf['log_level']
    log_name = (conf['log_name'] + schema).upper()
    converter = conf['converter']
    #
    # Set up logger
    log = setup_logging(log_name, log_level)
    log.info("INFO  ON")
    log.info("DEBUG ON")
    #
    # Print conf
    log.info(pprint.pformat(conf))

    src_schema = conf['schema_in']
    target_schema = conf['schema_out']
    schema_conf_file = os.path.abspath(conf_path + '/' + conf['config_file'])

    t0 = time.time()
    log.info(""" START MIGRATING {schema_in} to {schema_out} """.format(schema_in=src_schema
                                                                        , schema_out=target_schema))
    # connect to db
    pg = pg_connect(conf['postgres'])
    #
    # Set default search path for user to Schema Out
    set_search_path(pg, target_schema, log)
    #
    # Empty target schema before migrating
    empty_schema(pg, target_schema, log)
    #
    # Here call the converter.
    log.info(call_converter(converter=converter, conf=schema_conf_file, log=log, debug=True))
    dur = time.time() - t0
    for n in pg.notices:
        log.debug(n)
    #
    #
    grant_usage_target_schema(pg, target_schema, log)
    #
    #
    pg.close()
    log.info(""" DONE MIGRATING {schema_in} to {schema_out} -- Time: {dur} seconds""".format(
            schema_in=src_schema
            , schema_out=target_schema
            , dur=str(round(dur, 2))))

if __name__ == "__main__":
    do_migrate('../conf/do_migrate.yml', 'magento')
