#!/bin/bash

BASE_DIR="$( cd "$( dirname "$0" )" && pwd )";
QUERIES_DIR="$BASE_DIR/queries/*.sql";
echo ${0} ${*} ${#}

# cat $QUERIES | mysql -h127.0.0.1 -P3307 --user=root --force


echo -e "\n\n STARTING WAREHOUSE MIGRATION:  " `date "+%Y-%m-%d T %H:%M:%S"` && cat $QUERIES | mysql -h127.0.0.1 -P3307 --user=root --force && echo -e "\n DONE WAREHOUSE MIGRATION    :  " `date "+%Y-%m-%d T %H:%M:%S"`"\n\n"
