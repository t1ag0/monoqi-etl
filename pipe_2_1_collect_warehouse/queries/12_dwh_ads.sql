DROP TABLE IF EXISTS warehouse_dev.campaigns;

CREATE TABLE warehouse_dev.campaigns (
`id` int(10) unsigned NOT NULL,
PRIMARY KEY(`id`),
INDEX(`id`),
channel_name VARCHAR(255),
INDEX(channel_name),
name VARCHAR(255),
INDEX(name),
start_date DATETIME,
end_date DATETIME
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT (@id := @id + 1) AS id,
channel_name,
name,
start_date,
end_date

FROM (SELECT
utm_source.value AS channel_name,
SUBSTRING_INDEX(utm_campaign.value, '-', LENGTH(utm_campaign.value) - LENGTH(REPLACE(utm_campaign.value, '-', '')) - 2) AS name,

DATE(MIN(signup.signup_at)) AS start_date,
DATE_ADD(DATE(MAX(signup.signup_at)), INTERVAL 1 DAY) AS end_date

FROM monoqib2c.customer_entity customer

LEFT JOIN monoqib2c.customer_entity_varchar utm_campaign ON(customer.entity_id = utm_campaign.entity_id AND utm_campaign.attribute_id = 227)
LEFT JOIN monoqib2c.customer_entity_varchar utm_medium ON(customer.entity_id = utm_medium.entity_id AND utm_medium.attribute_id = 228)
LEFT JOIN monoqib2c.customer_entity_varchar utm_source ON(customer.entity_id = utm_source.entity_id AND utm_source.attribute_id = 229)
LEFT JOIN monoqib2c.customer_entity_varchar utm_content ON(customer.entity_id = utm_content.entity_id AND utm_content.attribute_id = 230)
LEFT JOIN warehouse_dev.customer_signups signup ON(signup.user_id = customer.entity_id)


WHERE utm_source.value = 'facebook'

AND SUBSTRING_INDEX( utm_content.value,  '-', -1 ) REGEXP '^-?[0-9]+$'

AND LENGTH(SUBSTRING_INDEX( utm_content.value,  '-', -1 )) = 13 

GROUP BY name) t1

JOIN(SELECT @id := 0) AS id_dummy
;


DROP TABLE IF EXISTS warehouse_dev.ads;


CREATE TABLE warehouse_dev.ads (
`id` int(10) unsigned NOT NULL,
PRIMARY KEY(`id`),
INDEX(`id`),
campaign_id INT(10) unsigned NOT NULL,
INDEX(campaign_id),
native_ad_id VARCHAR(255),
INDEX(native_ad_id),
name VARCHAR(255),
INDEX(name)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT (@id := @id + 1) AS id,
campaign_id,
native_ad_id,
name


FROM (SELECT
campaigns.id AS campaign_id,
(CASE WHEN utm_source.value = 'facebook' THEN SUBSTRING_INDEX( utm_content.value,  '-', -1 ) ELSE NULL END) AS native_ad_id,
(CASE WHEN utm_source.value = 'facebook' THEN REPLACE(utm_content.value, CONCAT('-', SUBSTRING_INDEX( utm_content.value,  '-', -1 )), '') ELSE NULL END) AS name


FROM monoqib2c.customer_entity customer

LEFT JOIN monoqib2c.customer_entity_varchar utm_campaign ON(customer.entity_id = utm_campaign.entity_id AND utm_campaign.attribute_id = 227)
LEFT JOIN monoqib2c.customer_entity_varchar utm_medium ON(customer.entity_id = utm_medium.entity_id AND utm_medium.attribute_id = 228)
LEFT JOIN monoqib2c.customer_entity_varchar utm_source ON(customer.entity_id = utm_source.entity_id AND utm_source.attribute_id = 229)
LEFT JOIN monoqib2c.customer_entity_varchar utm_content ON(customer.entity_id = utm_content.entity_id AND utm_content.attribute_id = 230)
LEFT JOIN warehouse_dev.campaigns campaigns ON(campaigns.name = SUBSTRING_INDEX(utm_campaign.value, '-', LENGTH(utm_campaign.value) - LENGTH(REPLACE(utm_campaign.value, '-', '')) - 2))


WHERE utm_source.value = 'facebook'

AND SUBSTRING_INDEX( utm_content.value,  '-', -1 ) REGEXP '^-?[0-9]+$'

AND LENGTH(SUBSTRING_INDEX( utm_content.value,  '-', -1 )) = 13 

GROUP BY native_ad_id

) t1

JOIN(SELECT @id := 0) AS id_dummy



;

DROP TABLE IF EXISTS warehouse_dev.target_groups;

CREATE TABLE warehouse_dev.target_groups (
`id` int(10) unsigned NOT NULL,
PRIMARY KEY(`id`),
INDEX(`id`),
`ad_id` int(10) unsigned NOT NULL,

INDEX(`ad_id`),
name VARCHAR(255),
INDEX(name),
gender VARCHAR(30),
INDEX(gender),
age_group VARCHAR(30),
INDEX(age_group)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT (@id := @id + 1) AS id,
ad_id,
name,
gender,
age_group

FROM (SELECT
ads.id AS ad_id,
SUBSTRING_INDEX(utm_campaign.value, '-', LENGTH(utm_campaign.value) - LENGTH(REPLACE(utm_campaign.value, '-', '')) - 2) AS name,

SUBSTRING_INDEX(SUBSTRING_INDEX(utm_campaign.value, '-', -3),'-', 1) AS gender,
SUBSTRING_INDEX(utm_campaign.value, '-', -2) AS age_group

FROM monoqib2c.customer_entity customer

LEFT JOIN monoqib2c.customer_entity_varchar utm_campaign ON(customer.entity_id = utm_campaign.entity_id AND utm_campaign.attribute_id = 227)
LEFT JOIN monoqib2c.customer_entity_varchar utm_medium ON(customer.entity_id = utm_medium.entity_id AND utm_medium.attribute_id = 228)
LEFT JOIN monoqib2c.customer_entity_varchar utm_source ON(customer.entity_id = utm_source.entity_id AND utm_source.attribute_id = 229)
LEFT JOIN monoqib2c.customer_entity_varchar utm_content ON(customer.entity_id = utm_content.entity_id AND utm_content.attribute_id = 230)
LEFT JOIN warehouse_dev.ads ads ON(ads.native_ad_id = SUBSTRING_INDEX( utm_content.value,  '-', -1 ))



WHERE utm_source.value = 'facebook'

AND SUBSTRING_INDEX( utm_content.value,  '-', -1 ) REGEXP '^-?[0-9]+$'

AND LENGTH(SUBSTRING_INDEX( utm_content.value,  '-', -1 )) = 13 

GROUP BY campaign_id, name, gender, age_group) t1

JOIN(SELECT @id := 0) AS id_dummy
;