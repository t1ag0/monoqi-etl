DROP TABLE IF EXISTS warehouse_dev.root_product;
DROP TABLE IF EXISTS warehouse_dev.products;

CREATE TABLE warehouse_dev.root_product 
(entity_id INTEGER NOT NULL, 
PRIMARY KEY(entity_id), 
INDEX(entity_id), 
root_product INTEGER NOT NULL, 
INDEX(root_product)) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT product.entity_id, (CASE WHEN relation.child_id IS NULL THEN product.entity_id ELSE relation.parent_id END) AS root_product
FROM monoqib2c.catalog_product_entity product
LEFT JOIN monoqib2c.catalog_product_relation relation ON(relation.child_id = product.entity_id)

GROUP BY entity_id;

CREATE TABLE IF NOT EXISTS warehouse_dev.products 
(id INTEGER NOT NULL, 
PRIMARY KEY(id), 
root_product_id INTEGER NOT NULL,  
INDEX(root_product_id), 
magento_shop_id INT(10),
INDEX(magento_shop_id),
created_at datetime DEFAULT NULL,
updated_at datetime DEFAULT NULL,
sku VARCHAR(30),
INDEX(sku),
evnr VARCHAR(30),
INDEX(evnr),
name VARCHAR(255),
designer_name VARCHAR(255),
brand_name VARCHAR(255),
category VARCHAR(255),
INDEX(category),
set_id INTEGER,
INDEX(set_id),
`net_retail` decimal(12,4) DEFAULT NULL,
`net_wholesale` decimal(12,4) DEFAULT NULL,
`original_net_retail` decimal(12,4) DEFAULT NULL,
`gross_retail` decimal(12,4) DEFAULT NULL,
`original_gross_retail` decimal(12,4) DEFAULT NULL,
`discount_rate` decimal(12,4) DEFAULT NULL,
`vat_rate` decimal(12,4) DEFAULT NULL,
gross_price_point VARCHAR(30),
initial_quantity INTEGER(30) NOT NULL,
additional_quantity INTEGER(30) NOT NULL,
latest_delivery_date DATE DEFAULT NULL,
shipping_mode VARCHAR(30) DEFAULT NULL,
carrier_id INTEGER(10) DEFAULT NULL,
INDEX(carrier_id),
carrier_name VARCHAR(255) DEFAULT NULL,
INDEX(carrier_name),
logistic_suffix VARCHAR(3) DEFAULT NULL,
INDEX(shipping_mode),
shop VARCHAR(30) NOT NULL, 
INDEX(shop),
shop_type VARCHAR(30) NOT NULL, 
INDEX(shop_type),
de_url VARCHAR(255),
INDEX (de_url),
en_url VARCHAR(255),
INDEX (en_url),
picture_url VARCHAR(255),
INDEX(picture_url)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


SELECT t1.*,
  CASE WHEN cat.parent_id = 3182 OR cat.parent_id = 6220 OR cat.parent_id = 6552 OR cat.parent_id = 7172 OR cat.parent_id = 5531 OR cat.parent_id = 4426 THEN cat.parent_id ELSE cat.entity_id END magento_shop_id,
  set_id.category_id AS set_id,
  evnr.value AS evnr,
  p_name.value AS name,
  d_name.value AS designer_name,
  brand_name.value AS brand_name,
  magento_wholesale.value AS net_wholesale,
  NULL AS category,
  carrier.value AS carrier_id,
  NULL AS carrier_name,
  CASE WHEN carrier.value = 327 OR set_id.category_id = 477 THEN 'P' ELSE 'F' END AS logistic_suffix,

  STR_TO_DATE(REPLACE(CONCAT(SUBSTRING_INDEX(latest_delivery.value, '.', 2), '-', SUBSTRING_INDEX(latest_delivery.value, '.', -1)  ),'.','-'),'%d-%m-%Y') AS latest_delivery_date,

  shipping_mode_name.value AS shipping_mode,
  NULL AS shop,
  NULL AS shop_type, -- TO BE SET IN THE DWH

  CONCAT('https://monoqi.com/media/catalog/product', CASE WHEN pic1.value IS NULL THEN pic2.value ELSE pic1.value END) AS picture_url,
  null AS de_url,
  null AS en_url
  FROM (
     SELECT
       product.entity_id AS id,
       root_product.root_product AS root_product_id,
       product.created_at,
       product.updated_at,
       product.sku,
       (mqprice.value / (CASE WHEN tax_class_id.value = 5 THEN 1.19 ELSE 1.07 END)) AS net_retail,
       (retail.value / (CASE WHEN tax_class_id.value = 5 THEN 1.19 ELSE 1.07 END)) AS original_net_retail,
       (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) AS gross_retail,
       (retail.value) AS original_gross_retail,
       (1 - ((CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END)/retail.value)) AS discount_rate,
       CONVERT(initial_quantity.value, UNSIGNED INTEGER) AS initial_quantity,
       (CASE WHEN CONVERT(additional_quantity.value, UNSIGNED INTEGER) = 2147483647 THEN 0 ELSE CONVERT(additional_quantity.value, UNSIGNED INTEGER) END) AS additional_quantity,
       (CASE WHEN tax_class_id.value = 5 THEN 0.19 ELSE 0.07 END) AS vat_rate,
       (CASE
        WHEN (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) < 50 THEN '0-50'
        WHEN (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) >= 50 AND (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) < 100 THEN '50-100'
        WHEN (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) >= 100 AND (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) < 200 THEN '100-200'
        WHEN (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) >= 200 AND (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) < 400 THEN '200-400'
        WHEN (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) >= 400 AND (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) < 700 THEN '400-700'
        WHEN (CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (retail.value) END) >= 700 THEN 'Over_700'
        END) AS gross_price_point

     FROM monoqib2c.catalog_product_entity product
       LEFT JOIN warehouse_dev.root_product root_product ON(root_product.entity_id = product.entity_id)
       LEFT JOIN monoqib2c.catalog_product_entity_decimal retail ON(retail.entity_id = root_product.root_product AND retail.attribute_id = 69)
       LEFT JOIN monoqib2c.catalog_product_entity_decimal mqprice ON(mqprice.entity_id = root_product.root_product AND mqprice.attribute_id = 70)
       LEFT JOIN monoqib2c.catalog_product_entity_int tax_class_id ON(tax_class_id.entity_id = root_product.root_product AND tax_class_id.attribute_id = 115)
       LEFT JOIN monoqib2c.catalog_product_entity_varchar initial_quantity ON(initial_quantity.entity_id = product.entity_id AND initial_quantity.attribute_id = 237 AND initial_quantity.store_id = 0)
       LEFT JOIN monoqib2c.catalog_product_entity_varchar additional_quantity ON(additional_quantity.entity_id = product.entity_id AND additional_quantity.attribute_id = 261 AND additional_quantity.store_id = 0)
     WHERE product.sku IS NOT NULL
     GROUP BY id
   ) as t1
  LEFT JOIN monoqib2c.catalog_product_entity_decimal magento_wholesale ON(magento_wholesale.entity_id = t1.root_product_id AND magento_wholesale.attribute_id = 236)
  LEFT JOIN monoqib2c.catalog_category_product set_id ON(set_id.product_id = t1.root_product_id)
  LEFT JOIN monoqib2c.catalog_category_entity cat ON (cat.entity_id = set_id.category_id)
  LEFT JOIN monoqib2c.catalog_product_entity_varchar p_name ON(p_name.entity_id = t1.root_product_id AND p_name.attribute_id = 65 AND p_name.store_id = 0)
  LEFT JOIN monoqib2c.catalog_product_entity_varchar latest_delivery ON(latest_delivery.entity_id = t1.root_product_id AND latest_delivery.attribute_id = 214 AND latest_delivery.store_id = 0)
  LEFT JOIN monoqib2c.catalog_product_entity_varchar evnr ON(evnr.entity_id = t1.root_product_id AND evnr.attribute_id = 222 AND evnr.store_id = 0)
  LEFT JOIN monoqib2c.catalog_product_entity_varchar brand_name ON(brand_name.entity_id = t1.root_product_id AND brand_name.attribute_id = 202 AND brand_name.store_id = 0)
  LEFT JOIN monoqib2c.catalog_category_entity_varchar d_name ON(d_name.entity_id = set_id.category_id AND d_name.entity_type_id = 3 AND d_name.attribute_id = 172 AND d_name.store_id = 0)
  LEFT JOIN monoqib2c.catalog_product_entity_int cat_id ON(cat_id.entity_id = t1.root_product_id AND cat_id.attribute_id = 243 AND cat_id.store_id = 0)
  LEFT JOIN monoqib2c.catalog_product_entity_int carrier ON (carrier.entity_id = t1.root_product_id AND carrier.attribute_id = 231 AND carrier.store_id = 0)
  LEFT JOIN monoqib2c.eav_attribute_option_value cat_name ON(cat_name.option_id = cat_id.value AND cat_name.store_id = 0)
  LEFT JOIN monoqib2c.catalog_product_entity_varchar cat_name2 ON (cat_name2.entity_id = t1.root_product_id AND cat_name2.attribute_id = 238 AND cat_name2.store_id = 0)
  LEFT JOIN monoqib2c.catalog_product_entity_int shipping_mode ON(shipping_mode.entity_id = t1.id AND shipping_mode.attribute_id = 217 AND shipping_mode.store_id = 0)
  LEFT JOIN monoqib2c.eav_attribute_option_value shipping_mode_name ON(shipping_mode_name.option_id = shipping_mode.value AND shipping_mode_name.store_id = 0)
  LEFT JOIN monoqib2c.catalog_product_entity_varchar pic1 ON(pic1.entity_id = t1.root_product_id AND pic1.attribute_id = 80 AND pic1.store_id = 0)
  LEFT JOIN monoqib2c.catalog_product_entity_varchar pic2 ON(pic2.entity_id = t1.root_product_id AND pic2.attribute_id = 79 AND pic2.store_id = 0)
GROUP BY id;

UPDATE warehouse_dev.products AS products
  LEFT JOIN monoqib2c.core_url_rewrite AS de_url ON de_url.product_id = products.root_product_id
SET
  de_url = CONCAT('https://monoqi.com/de/', de_url.request_path)
WHERE de_url.is_system = 1 AND de_url.store_id = 1 AND de_url.category_id IS NOT NULL;

UPDATE warehouse_dev.products AS products
  LEFT JOIN monoqib2c.core_url_rewrite AS en_url ON en_url.product_id = products.root_product_id
SET
  en_url = CONCAT('https://monoqi.com/en/', en_url.request_path)
WHERE en_url.is_system = 1 AND en_url.store_id = 2 AND en_url.category_id IS NOT NULL;


DROP TABLE IF EXISTS warehouse_dev.product_prices;
CREATE TABLE warehouse_dev.product_prices
(id INTEGER NOT NULL, 
sku VARCHAR(30),
INDEX(sku),
website VARCHAR(255),
INDEX(website),
PRIMARY KEY(id, website), 
currency VARCHAR(5),
INDEX(currency),
`net_retail` decimal(12,4) DEFAULT NULL,
`net_wholesale` decimal(12,4) DEFAULT NULL,
`original_net_retail` decimal(12,4) DEFAULT NULL,
`gross_retail` decimal(12,4) DEFAULT NULL,
`original_gross_retail` decimal(12,4) DEFAULT NULL,
`discount_rate` decimal(12,4) DEFAULT NULL,
`vat_rate` decimal(12,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT product.entity_id AS id, product.sku, website.name AS website,
CASE website.website_id WHEN 3 THEN 'GBP' WHEN 2 THEN 'CHF' ELSE 'EUR' END AS currency,
((CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (COALESCE(retail.value, default_retail.value)) END) / (1 + COALESCE(rate.rate, default_rate.rate)/100)) AS net_retail,
COALESCE(magento_wholesale.value, default_magento_wholesale.value) AS net_wholesale,
(COALESCE(retail.value, default_retail.value) / (1 + COALESCE(rate.rate, default_rate.rate)/100)) AS original_net_retail,
(CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (COALESCE(retail.value, default_retail.value)) END) AS gross_retail,
(COALESCE(retail.value, default_retail.value)) AS original_gross_retail,
(1 - ((CASE WHEN mqprice.value > 0 THEN (mqprice.value) ELSE (COALESCE(retail.value, default_retail.value)) END)/COALESCE(retail.value, default_retail.value))) AS discount_rate,
COALESCE(rate.rate, default_rate.rate)/100 AS vat_rate

FROM monoqib2c.catalog_product_entity product
LEFT JOIN warehouse_dev.root_product root_product ON(root_product.entity_id = product.entity_id)
LEFT JOIN monoqib2c.catalog_product_entity_decimal mqprice ON(mqprice.entity_id = root_product.root_product AND mqprice.attribute_id = 70)
LEFT JOIN monoqib2c.catalog_product_entity_decimal magento_wholesale ON(magento_wholesale.entity_id = root_product.root_product AND magento_wholesale.attribute_id = 236 AND mqprice.store_id = magento_wholesale.store_id)
LEFT JOIN monoqib2c.catalog_product_entity_decimal default_magento_wholesale ON(default_magento_wholesale.entity_id = root_product.root_product AND default_magento_wholesale.attribute_id = 236 AND default_magento_wholesale.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_decimal retail ON(retail.entity_id = root_product.root_product AND retail.attribute_id = 69 AND mqprice.store_id = retail.store_id)
LEFT JOIN monoqib2c.catalog_product_entity_decimal default_retail ON(default_retail.entity_id = root_product.root_product AND default_retail.attribute_id = 69 AND default_retail.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_int tax_class_id ON(tax_class_id.entity_id = root_product.root_product AND tax_class_id.attribute_id = 115 AND mqprice.store_id = tax_class_id.store_id)
LEFT JOIN monoqib2c.catalog_product_entity_int default_tax_class_id ON(default_tax_class_id.entity_id = root_product.root_product AND default_tax_class_id.attribute_id = 115 AND default_tax_class_id.store_id = 0)
LEFT JOIN monoqib2c.core_store store ON mqprice.store_id = store.store_id
LEFT JOIN monoqib2c.core_website website ON website.website_id = store.website_id
LEFT JOIN (
  SELECT
    rate.tax_calculation_rate_id,
    rate.tax_country_id,
    CASE rate.tax_country_id WHEN 'GB' THEN 3 WHEN 'CH' THEN 2 WHEN 'US' THEN 4 ELSE 1 END AS website_id,
    rate.rate,
    calculation.product_tax_class_id
  FROM monoqib2c.tax_calculation_rate as rate
  LEFT JOIN monoqib2c.tax_calculation as calculation ON calculation.tax_calculation_rate_id = rate.tax_calculation_rate_id
  WHERE rate.tax_country_id IN ('DE','CH','GB', 'US') AND calculation.product_tax_class_id IS NOT NULL
  GROUP BY rate.tax_calculation_rate_id, calculation.product_tax_class_id
) as rate ON rate.product_tax_class_id = tax_class_id.value AND rate.website_id = website.website_id
LEFT JOIN (
  SELECT
    rate.tax_calculation_rate_id,
    rate.tax_country_id,
    CASE rate.tax_country_id WHEN 'GB' THEN 3 WHEN 'CH' THEN 2 WHEN 'US' THEN 4 ELSE 1 END AS website_id,
    rate.rate,
    calculation.product_tax_class_id
  FROM monoqib2c.tax_calculation_rate as rate
  LEFT JOIN monoqib2c.tax_calculation as calculation ON calculation.tax_calculation_rate_id = rate.tax_calculation_rate_id
  WHERE rate.tax_country_id IN ('DE','CH','GB', 'US') AND calculation.product_tax_class_id IS NOT NULL
  GROUP BY rate.tax_calculation_rate_id, calculation.product_tax_class_id
) as default_rate ON default_rate.product_tax_class_id = default_tax_class_id.value AND default_rate.tax_country_id = 'DE'

WHERE product.sku IS NOT NULL AND mqprice.store_id != 4
GROUP BY product.entity_id, website.website_id
ORDER BY product.entity_id DESC;

DROP TABLE warehouse_dev.root_product;