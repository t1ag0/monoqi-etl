DROP TABLE IF EXISTS warehouse_dev.sets;
CREATE TABLE warehouse_dev.sets
(id INTEGER NOT NULL, 
PRIMARY KEY(id), 
INDEX(id), 
magento_shop_id INT(10),
INDEX(magento_shop_id),
created_at DATETIME DEFAULT NULL,
updated_at DATETIME DEFAULT NULL,
set_name VARCHAR(60), 
designer_name VARCHAR(60),
start_at DATETIME,
end_at DATETIME,
de_url VARCHAR(255),
en_url VARCHAR(255),
picture_url VARCHAR(255)) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT set_id.entity_id AS id,
CASE WHEN set_id.parent_id = 3182 OR set_id.parent_id = 6220 OR set_id.parent_id = 6552 OR set_id.parent_id = 7172 OR set_id.parent_id = 5531 OR set_id.parent_id = 4426 THEN set_id.parent_id ELSE set_id.entity_id END AS magento_shop_id,
set_id.created_at,
set_id.updated_at,
set_name.value AS set_name,
designer_name.value AS designer_name,
start_date.value AS start_at,
end_date.value AS end_at,
CONCAT('https://monoqi.com/media/catalog/category/', pic.value) AS picture_url,
NULL AS de_url,
NULL AS en_url
FROM monoqib2c.catalog_category_entity set_id
  LEFT JOIN monoqib2c.catalog_category_entity_varchar set_name ON(set_name.entity_id = set_id.entity_id AND set_name.entity_type_id = 3 AND set_name.attribute_id = 35 AND set_name.store_id = 0)
  LEFT JOIN monoqib2c.catalog_category_entity_varchar designer_name ON(designer_name.entity_id = set_id.entity_id AND designer_name.entity_type_id = 3 AND designer_name.attribute_id = 172 AND designer_name.store_id = 0)
  LEFT JOIN monoqib2c.catalog_category_entity_datetime start_date ON(start_date.entity_id = set_id.entity_id AND start_date.entity_type_id = 3 AND start_date.attribute_id = 289 AND start_date.store_id = 0)
  LEFT JOIN monoqib2c.catalog_category_entity_datetime end_date ON(end_date.entity_id = set_id.entity_id AND end_date.entity_type_id = 3 AND end_date.attribute_id = 290 AND end_date.store_id =0)
  LEFT JOIN monoqib2c.catalog_category_entity_varchar pic ON(pic.entity_id = set_id.entity_id AND pic.entity_type_id = 3 AND pic.attribute_id = 39 AND pic.store_id = 0);

UPDATE warehouse_dev.sets as main_table
  JOIN monoqib2c.core_url_rewrite as url_rewrite ON main_table.id = url_rewrite.category_id
SET main_table.de_url = CONCAT('https://monoqi.com/de/', url_rewrite.request_path)
WHERE url_rewrite.is_system = 1
  AND url_rewrite.store_id = 1
  AND url_rewrite.product_id IS NULL;

UPDATE warehouse_dev.sets as main_table
  JOIN monoqib2c.core_url_rewrite as url_rewrite ON main_table.id = url_rewrite.category_id
SET main_table.en_url = CONCAT('https://monoqi.com/en/', url_rewrite.request_path)
WHERE url_rewrite.is_system = 1
  AND url_rewrite.store_id = 2
  AND url_rewrite.product_id IS NULL;
