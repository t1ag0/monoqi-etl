DROP TABLE IF EXISTS warehouse_dev.shipping_configuration;

CREATE TABLE warehouse_dev.shipping_configuration
(id INTEGER ,
  `country` VARCHAR(70),
  `store` VARCHAR(70),
  `fall` VARCHAR(70),
  `mode` VARCHAR(70),
  `class` VARCHAR(70),
  `amount` VARCHAR(70) DEFAULT 0,
  PRIMARY KEY (`id`),
  INDEX `country` (`country`),
  INDEX `store` (`store`),
  INDEX `fall` (`fall`),
  INDEX `mode` (`mode`),
  INDEX `class` (`class`),
  INDEX `amount` (`amount`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT * FROM monoqib2c.innobyte_shipping;