DROP TABLE IF EXISTS warehouse_dev.set_attributes;

CREATE TABLE warehouse_dev.set_attributes 
(id INTEGER NOT NULL, 
PRIMARY KEY(id),
en_name VARCHAR(255),
de_name VARCHAR(255),
en_brand_name VARCHAR(255),
de_brand_name VARCHAR(255),
en_description TEXT,
de_description TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT set_id.entity_id AS id,
(CASE WHEN en_name.value IS NOT NULL THEN en_name.value ELSE default_name.value END) AS en_name,
(CASE WHEN de_name.value IS NOT NULL THEN de_name.value ELSE default_name.value END) AS de_name,
(CASE WHEN en_brand_name.value IS NOT NULL THEN en_brand_name.value ELSE default_brand_name.value END) AS en_brand_name,
(CASE WHEN de_brand_name.value IS NOT NULL THEN de_brand_name.value ELSE default_brand_name.value END) AS de_brand_name,
(CASE WHEN en_description.value IS NOT NULL THEN en_description.value ELSE default_description.value END) AS en_description,
(CASE WHEN de_description.value IS NOT NULL THEN de_description.value ELSE default_description.value END) AS de_description

FROM monoqib2c.catalog_category_entity set_id 
LEFT JOIN monoqib2c.catalog_category_entity_varchar en_name ON(en_name.entity_id = set_id.entity_id AND en_name.attribute_id = 35 AND en_name.store_id = 2)
LEFT JOIN monoqib2c.catalog_category_entity_varchar de_name ON(de_name.entity_id = set_id.entity_id AND de_name.attribute_id = 35 AND de_name.store_id = 1)
LEFT JOIN monoqib2c.catalog_category_entity_varchar default_name ON(default_name.entity_id = set_id.entity_id AND default_name.attribute_id = 35 AND default_name.store_id = 0)
LEFT JOIN monoqib2c.catalog_category_entity_varchar en_brand_name ON(en_brand_name.entity_id = set_id.entity_id AND en_brand_name.attribute_id = 172 AND en_brand_name.store_id = 2)
LEFT JOIN monoqib2c.catalog_category_entity_varchar de_brand_name ON(de_brand_name.entity_id = set_id.entity_id AND de_brand_name.attribute_id = 172 AND de_brand_name.store_id = 1)
LEFT JOIN monoqib2c.catalog_category_entity_varchar default_brand_name ON(default_brand_name.entity_id = set_id.entity_id AND default_brand_name.attribute_id = 172 AND default_brand_name.store_id = 0)

LEFT JOIN monoqib2c.catalog_category_entity_text de_description ON(de_description.entity_id = set_id.entity_id AND de_description.attribute_id = 38 AND de_description.store_id = 1)
LEFT JOIN monoqib2c.catalog_category_entity_text en_description ON(en_description.entity_id = set_id.entity_id AND en_description.attribute_id = 38 AND en_description.store_id = 2)
LEFT JOIN monoqib2c.catalog_category_entity_text default_description ON(default_description.entity_id = set_id.entity_id AND default_description.attribute_id = 38 AND default_description.store_id = 0)
;