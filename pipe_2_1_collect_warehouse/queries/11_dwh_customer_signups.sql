DROP TABLE IF EXISTS warehouse_dev.customers;
DROP TABLE IF EXISTS warehouse_dev.customer_address_id;
DROP TABLE IF EXISTS warehouse_dev.customer_address_2;
DROP TABLE IF EXISTS warehouse_dev.customer_address_3;

CREATE TABLE warehouse_dev.customer_address_id (
`address_id` int(10) unsigned NOT NULL,
PRIMARY KEY(`address_id`),
INDEX(`address_id`),
`entity_id` int(10) unsigned NOT NULL,

INDEX(`entity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT MAX(`entity_id`) AS address_id, `parent_id` AS entity_id FROM monoqib2c.`customer_address_entity` GROUP BY parent_id;


CREATE TABLE warehouse_dev.customer_address_2 (
`entity_id` int(10) unsigned NOT NULL,
PRIMARY KEY(`entity_id`),
INDEX(`entity_id`),
`city` VARCHAR(255) DEFAULT NULL,
`country_id` VARCHAR(255) DEFAULT NULL,
`telephone` VARCHAR(255) DEFAULT NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT `customer_id` AS entity_id, `city`, `country_id`, `telephone` FROM 

(SELECT * FROM monoqib2c.`sales_flat_order_address` WHERE (country_id IS NOT NULL) ORDER BY entity_id DESC) t1

GROUP BY customer_id;

CREATE TABLE warehouse_dev.customer_address_3 (
`entity_id` int(10) unsigned NOT NULL,
PRIMARY KEY(`entity_id`),
INDEX(`entity_id`),
`city` VARCHAR(255) DEFAULT NULL,
`country_id` VARCHAR(255) DEFAULT NULL,
`telephone` VARCHAR(255) DEFAULT NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT `customer_id` AS entity_id, `city`, `country_id`, `telephone` FROM 

(SELECT * FROM monoqib2c.`sales_flat_quote_address` WHERE (country_id IS NOT NULL) ORDER BY created_at DESC) t1

GROUP BY customer_id;




DROP TABLE IF EXISTS warehouse_dev.customer_signups;
DROP TABLE IF EXISTS warehouse_dev.customer_signups_temp;
DROP TABLE IF EXISTS warehouse_dev.customer_signups_temp2;
DROP TABLE IF EXISTS warehouse_dev.first_order_at_temp;

CREATE TABLE warehouse_dev.first_order_at_temp (
`customer_email` varchar(255),
PRIMARY KEY(`customer_email`),
first_order_at DATETIME

) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT customer_email, MIN(created_at) AS first_order_at FROM monoqib2c.sales_flat_order GROUP BY customer_email;

SET @created = '2012-01-24 00:00:00';

-- checks if the customer has a higher signup date than the previous ID
CREATE TABLE IF NOT EXISTS warehouse_dev.customer_signups_temp (
`id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
PRIMARY KEY(`id`),
INDEX(`id`),
`email` varchar(255) DEFAULT NULL COMMENT 'Email',
`created_at` datetime DEFAULT NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT customer.entity_id AS id,  
(@created := (CASE WHEN DATEDIFF(created_at,@created) > 1 OR DATEDIFF(created_at,@created) < 0 THEN @created ELSE created_at END)) AS created_at, 
DATE_SUB(created_at, INTERVAL DATEDIFF((created_at),(@created)) DAY) AS signup_at
FROM monoqib2c.customer_entity customer
WHERE customer.entity_id > 9999;

-- checks that signup date is lower than confirm date
CREATE TABLE IF NOT EXISTS warehouse_dev.customer_signups_temp2 (
`user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
PRIMARY KEY(`user_id`),
INDEX(`user_id`),
`signup_at` datetime DEFAULT NULL,
`signup_attribute` datetime DEFAULT NULL,
`confirm_attribute` datetime DEFAULT NULL,
`first_order_at` datetime DEFAULT NULL

) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT id AS user_id, 
TIMESTAMP((SELECT @created := (CASE WHEN ((UNIX_TIMESTAMP(@created) - UNIX_TIMESTAMP(c.created_at)) < 0 AND (confirm.value IS NULL OR UNIX_TIMESTAMP(confirm.value) - UNIX_TIMESTAMP(c.created_at) > 0)) THEN c.created_at ELSE @created END))) AS signup_at, 
signup.value AS signup_attribute, 
confirm.value AS confirm_attribute,
first_order_at

FROM warehouse_dev.customer_signups_temp c
LEFT JOIN monoqib2c.customer_entity email ON(email.entity_id = c.id)

LEFT JOIN monoqib2c.customer_entity_varchar confirm ON(confirm.entity_id = c.id AND confirm.attribute_id = 178)
LEFT JOIN monoqib2c.customer_entity_datetime signup ON(signup.entity_id = c.id AND signup.attribute_id = 284)
LEFT JOIN warehouse_dev.first_order_at_temp fo ON(fo.customer_email = email.email)

JOIN (SELECT @created := '2012-01-24 00:00:00') dummy

GROUP BY id;

DROP TABLE IF EXISTS warehouse_dev.customer_signups;


CREATE TABLE IF NOT EXISTS warehouse_dev.customer_signups (
`user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
PRIMARY KEY(`user_id`),
INDEX(`user_id`),
`signup_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT user_id, 

LEAST(
(CASE WHEN signup_attribute < '2012-01-01 00:00:00' OR signup_attribute IS NULL THEN '2999-01-01' ELSE signup_attribute END),
(CASE WHEN confirm_attribute < '2012-01-01 00:00:00' OR confirm_attribute IS NULL THEN '2999-01-01' ELSE confirm_attribute END),
(CASE WHEN first_order_at < '2012-01-01 00:00:00' OR first_order_at IS NULL THEN '2999-01-01' ELSE first_order_at END),
(CASE WHEN signup_at < '2012-01-01 00:00:00' OR signup_at IS NULL THEN '2999-01-01' ELSE signup_at END)) 

AS signup_at

FROM warehouse_dev.customer_signups_temp2

GROUP BY user_id;

DROP TABLE IF EXISTS warehouse_dev.first_order_at_temp;
DROP TABLE IF EXISTS warehouse_dev.customer_signups_temp;
DROP TABLE IF EXISTS warehouse_dev.customer_signups_temp2;