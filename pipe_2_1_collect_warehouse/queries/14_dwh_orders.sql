-- ROOT PRODUCTS

DROP TABLE IF EXISTS warehouse_dev.root_product;

CREATE TABLE warehouse_dev.root_product (
entity_id INTEGER NOT NULL, 
PRIMARY KEY(entity_id), 
root_product_id INTEGER NOT NULL, 
INDEX(root_product_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT product.entity_id, 
MAX(CASE WHEN relation.child_id IS NULL THEN product.entity_id ELSE relation.parent_id END) AS root_product_id

FROM monoqib2c.catalog_product_entity product
LEFT JOIN monoqib2c.catalog_product_relation relation  ON(relation.child_id = product.entity_id)

GROUP BY product.entity_id;



-- AVOID DUPLICATED ROWS FOR CONFIG PRODUCTS BY FILTERING OUT THE MOTHER PRODUCT

DROP TABLE IF EXISTS warehouse_dev.temp_flat_item_child_id;

CREATE TABLE warehouse_dev.temp_flat_item_child_id (
item_id INTEGER NOT NULL, 
PRIMARY KEY(item_id), 
child_item_id INTEGER NOT NULL, 
INDEX(child_item_id),
product_id INTEGER DEFAULT NULL, 
INDEX(product_id)) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT product_orders.parent_item_id AS item_id,
product_orders.item_id AS child_item_id,
product_orders.product_id

FROM monoqib2c.sales_flat_order_item product_orders

WHERE item_id != parent_item_id AND parent_item_id IS NOT NULL;


-- GET THE LAST INVOICE FOR ORDERS

DROP TABLE IF EXISTS warehouse_dev.temp_invoice;

CREATE TABLE warehouse_dev.temp_invoice (
order_id INTEGER NOT NULL, 
PRIMARY KEY(order_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT order_id, 
	MAX(base_customer_credit) AS base_customer_credit,
	MAX(increment_id) AS increment_id 

FROM monoqib2c.sales_flat_invoice 

GROUP BY order_id;





-- ORDERS

SET @order_nr = 0;
SET @customer_id = 10000;
SET @last_ordered_at = NULL;

DROP TABLE IF EXISTS warehouse_dev.customer_orders;

CREATE TABLE warehouse_dev.customer_orders
(id INTEGER(30) NOT NULL,
PRIMARY KEY(id),
magento_order_number VARCHAR(30) NOT NULL, 
INDEX(magento_order_number),

invoice_number VARCHAR(30) NOT NULL, 
INDEX(invoice_number),
`store_id` INTEGER DEFAULT NULL,
INDEX(store_id),
store_name VARCHAR(255) DEFAULT NULL,
customer_id INTEGER NOT NULL, 
INDEX(customer_id),
order_number INTEGER,
INDEX(order_number),
`ordered_at` datetime DEFAULT NULL,
`status` varchar(32) DEFAULT NULL,
INDEX(status),
`total_quantity` decimal(12,4) DEFAULT NULL,
`net_retail` decimal(12,2) DEFAULT NULL,
`net_shipping` decimal(12,2) DEFAULT NULL,
`net_wholesale` decimal(12,2) DEFAULT NULL,
`net_discount` decimal(12,2) DEFAULT NULL,
`net_credits_used` decimal(12,2) DEFAULT NULL,
`average_vat_rate` decimal(12,4) DEFAULT NULL,
`total_vat` decimal(12,2) DEFAULT NULL,
`retail_vat` decimal(12,2) DEFAULT NULL,
`shipping_vat` decimal(12,2) DEFAULT NULL,
`discount_vat` decimal(12,2) DEFAULT NULL,
`gross_retail` decimal(12,2) DEFAULT NULL,
`gross_shipping` decimal(12,2) DEFAULT NULL,
`gross_discount` decimal(12,2) DEFAULT NULL,
`gross_credits_used` decimal(12,2) DEFAULT NULL,
`gross_original_retail_value` decimal(12,2) DEFAULT NULL,
`net_original_retail_value` decimal(12,2) DEFAULT NULL,
`discount_rate` decimal(12,4) DEFAULT NULL,
`refunded_gross_retail` decimal(12,4) DEFAULT NULL,
`refunded_net_retail` decimal(12,4) DEFAULT NULL,
`refunded_gross_shipping` decimal(12,4) DEFAULT NULL,
`refunded_net_shipping` decimal(12,4) DEFAULT NULL,
  base_currency_code  varchar(30) DEFAULT NULL,
  total_due decimal(12,4) DEFAULT NULL,
  total_refunded decimal(12,4) DEFAULT NULL,
  total_paid decimal(12,4) DEFAULT NULL,
  payone_transaction_status varchar(30) DEFAULT NULL,
  shipment_status varchar(30) DEFAULT NULL,
`payment_method` varchar(30) DEFAULT NULL,
`cc_type` varchar(30) DEFAULT NULL,
INDEX(payment_method),
INDEX(cc_type),
`coupon_code` varchar(30) DEFAULT NULL,
billing_name VARCHAR(255),
billing_first_name VARCHAR(255),
billing_last_name VARCHAR(255),
billing_company VARCHAR(255),
billing_address VARCHAR(255),
billing_city VARCHAR(255),
billing_post_code VARCHAR(255),
billing_country VARCHAR(255),
billing_telephone VARCHAR(255),
billing_email VARCHAR(255),

shipping_name VARCHAR(255),
shipping_first_name VARCHAR(255),
shipping_last_name VARCHAR(255),
shipping_company VARCHAR(255),
shipping_address VARCHAR(255),
shipping_city VARCHAR(255),
shipping_post_code VARCHAR(255),
shipping_country VARCHAR(255),
shipping_telephone VARCHAR(255),
shipping_email VARCHAR(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8


SELECT id,
magento_order_number,
invoice_number,
store_id,
NULL AS store_name, -- TO BE SET IN THE DWH
customer_id,
(CASE WHEN status IN('processing','complete') THEN order_nr ELSE NULL END) AS order_number,
ordered_at,
time_since_signup,
(CASE WHEN (status NOT IN('processing','complete') OR order_nr = 1) THEN NULL ELSE time_since_last_order END) AS time_since_last_order,
status,
total_quantity,
net_retail,
net_shipping,
net_wholesale,
net_discount,
net_credits_used,
(gross_retail / net_retail) - 1 AS average_vat_rate,
total_vat,
retail_vat,
shipping_vat,
discount_vat,
gross_retail,
gross_shipping,
gross_discount,
gross_credits_used,
gross_original_retail_value,
net_original_retail_value,
1 - (net_retail / net_original_retail_value) AS discount_rate,
refunded_gross_retail,
refunded_net_retail,
refunded_gross_shipping,
refunded_net_shipping,
  base_currency_code,
  total_due,
  total_refunded,
  total_paid,
  payone_transaction_status,
  shipment_status,
  payment_method,
cc_type,
coupon_code,
billing_name,
billing_first_name,
billing_last_name,
billing_company,
billing_address,
billing_city,
billing_post_code,
billing_country,
billing_telephone,
billing_email,

shipping_name,
shipping_first_name,
shipping_last_name,
shipping_company,
shipping_address,
shipping_city,
shipping_post_code,
shipping_country,
shipping_telephone,
shipping_email

FROM

(
SELECT *, 

(@order_nr := CASE 
WHEN @customer_id != customer_id AND status IN('processing', 'complete') THEN 1
WHEN @customer_id != customer_id AND status != 'processing' AND status != 'complete' THEN NULL
WHEN @order_nr IS NULL AND status IN('processing', 'complete') THEN 1
WHEN @order_nr IS NOT NULL AND status IN('processing', 'complete') THEN @order_nr + 1
WHEN @order_nr IS NOT NULL AND status != 'processing' AND status != 'complete' THEN @order_nr
ELSE NULL END) AS order_nr,

(CASE 
WHEN (@customer_id != customer_id OR @last_ordered_at IS NULL OR (status != 'processing' AND status != 'complete')) THEN NULL 
ELSE UNIX_TIMESTAMP(ordered_at)-UNIX_TIMESTAMP(@last_ordered_at) END) AS time_since_last_order,

(@last_ordered_at := CASE 
WHEN status IN('processing','complete') THEN ordered_at ELSE @last_ordered_at END) AS last_ordered_at,

(@customer_id := customer_id)

FROM

(
SELECT orders.entity_id AS id,
orders.increment_id AS magento_order_number,
invoice.increment_id AS invoice_number,
orders.store_id AS store_id,
orders.customer_id AS customer_id,
orders.created_at AS ordered_at,
UNIX_TIMESTAMP(orders.created_at)-UNIX_TIMESTAMP(signup.signup_at) AS time_since_signup,
orders.status,
orders.total_qty_ordered AS total_quantity,
(orders.base_grand_total - orders.base_shipping_incl_tax + IFNULL(invoice.base_customer_credit, 0)) / (orders.base_subtotal_incl_tax / orders.base_subtotal) AS net_retail,
orders.base_shipping_amount AS net_shipping,
SUM(product_orders.qty_ordered * (CASE WHEN mei_wholesale.final_wholesale IS NOT NULL THEN mei_wholesale.final_wholesale ELSE magento_wholesale.value END)) AS net_wholesale,
ABS(orders.base_discount_amount) / (orders.base_subtotal_incl_tax / orders.base_subtotal) AS net_discount,
IFNULL(invoice.base_customer_credit, 0) / (orders.base_subtotal_incl_tax / orders.base_subtotal) AS net_credits_used,
IFNULL(invoice.base_customer_credit, 0) AS gross_credits_used,
-- (orders.tax_amount - orders.shipping_tax_amount) / orders.subtotal AS average_vat_rate,
(orders.base_grand_total - orders.base_shipping_incl_tax) * (1 - orders.base_subtotal / orders.base_subtotal_incl_tax) + orders.base_shipping_tax_amount AS total_vat,
(orders.base_grand_total - orders.base_shipping_incl_tax) * (1 - orders.base_subtotal / orders.base_subtotal_incl_tax) AS retail_vat,
orders.base_shipping_tax_amount AS shipping_vat,
ABS(orders.base_discount_amount) * (1 - orders.base_subtotal / orders.base_subtotal_incl_tax) AS discount_vat,
orders.base_grand_total - orders.base_shipping_incl_tax + IFNULL(invoice.base_customer_credit, 0) AS gross_retail,
orders.base_shipping_incl_tax AS gross_shipping,
ABS(orders.base_discount_amount) AS gross_discount,
SUM(product_orders.qty_ordered * COALESCE(original_retail.value, original_retail_old.value)) AS gross_original_retail_value,
SUM((product_orders.qty_ordered * COALESCE(original_retail.value, original_retail_old.value)) / (1 + product_orders.tax_percent / 100)) AS net_original_retail_value,
1 - SUM(product_orders.row_total / COALESCE(original_retail.value, original_retail_old.value)) AS discount_rate,
payment.method AS payment_method,
orders.subtotal_refunded + orders.tax_refunded AS  refunded_gross_retail,
orders.subtotal_refunded AS  refunded_net_retail,
orders.shipping_refunded  + orders.shipping_tax_refunded AS  refunded_gross_shipping,
orders.shipping_refunded AS  refunded_net_shipping,
orders.base_currency_code AS base_currency_code,
orders.total_due,
orders.total_refunded,
orders.total_paid,
orders.payone_transaction_status,
orders.shipment_status,
CASE WHEN payment.cc_type = 'A' THEN 'American Express' WHEN payment.cc_type = 'M' THEN 'Master' WHEN payment.cc_type = 'V' THEN 'Visa' ELSE NULL END AS cc_type,
orders.coupon_code,
CONCAT(billing.firstname, ' ', billing.lastname) AS billing_name,
billing.firstname AS billing_first_name,
billing.lastname AS billing_last_name,
billing.company AS billing_company,
billing.street AS billing_address,
billing.city AS billing_city,
billing.postcode AS billing_post_code,
billing.country_id AS billing_country,
billing.telephone AS billing_telephone,
billing.email AS billing_email,

CONCAT(shipping.firstname, ' ', shipping.lastname) AS shipping_name,
shipping.firstname AS shipping_first_name,
shipping.lastname AS shipping_last_name,
shipping.company AS shipping_company,
shipping.street AS shipping_address,
shipping.city AS shipping_city,
shipping.postcode AS shipping_post_code,
shipping.country_id AS shipping_country,
shipping.telephone AS shipping_telephone,
shipping.email AS shipping_email

FROM monoqib2c.sales_flat_order orders 
LEFT JOIN warehouse_dev.temp_invoice invoice ON(invoice.order_id = orders.entity_id)
LEFT JOIN monoqib2c.sales_flat_order_payment payment ON(payment.parent_id = orders.entity_id)
LEFT JOIN monoqib2c.sales_flat_order_address billing ON(billing.parent_id = orders.entity_id AND billing.address_type = 'billing')
LEFT JOIN monoqib2c.sales_flat_order_address shipping ON(shipping.parent_id = orders.entity_id AND shipping.address_type = 'shipping')
LEFT JOIN monoqib2c.sales_flat_order_item product_orders ON(orders.entity_id = product_orders.order_id)
LEFT JOIN monoqib2c.catalog_product_entity products ON(products.entity_id = product_orders.product_id)
LEFT JOIN warehouse_dev.root_product root_products ON(products.entity_id = root_products.entity_id)
LEFT JOIN monoqib2c.catalog_product_entity_decimal original_retail ON(original_retail.entity_id = root_products.root_product_id AND original_retail.attribute_id = 69 AND original_retail.store_id = orders.store_id)
LEFT JOIN monoqib2c.catalog_product_entity_decimal original_retail_old ON(original_retail_old.entity_id = root_products.root_product_id AND original_retail_old.attribute_id = 69 AND original_retail_old.store_id = 0) -- ORIGINAL RETAIL VALUE -> ONE BY STORE SINCE SWISS SHOP, STORE_ID = 0 BEFORE
LEFT JOIN warehouse_dev.20140127_wholesale_prices_fix_final mei_wholesale ON(mei_wholesale.final_sku = products.sku)
LEFT JOIN monoqib2c.catalog_product_entity_decimal magento_wholesale ON(magento_wholesale.entity_id = root_products.root_product_id AND magento_wholesale.attribute_id = 236 AND magento_wholesale.store_id = 0) -- WHOLESALE VALUE -> UNIQUE, STORE_ID = 0, VALUE IN EUROS
LEFT JOIN warehouse_dev.temp_flat_item_child_id child_item ON(child_item.item_id = product_orders.item_id)
LEFT JOIN warehouse_dev.customers signup ON(orders.customer_id = signup.id)

WHERE product_orders.parent_item_id IS NULL

GROUP BY orders.entity_id

ORDER BY customer_id, ordered_at ASC LIMIT 0,9999999999
) t1
) t2;


-- PRODUCT ORDERS

DROP TABLE IF EXISTS warehouse_dev.customer_product_orders;

CREATE TABLE warehouse_dev.customer_product_orders
(id INTEGER(30) NOT NULL AUTO_INCREMENT,
PRIMARY KEY(id),
customer_order_id INTEGER(30),
INDEX(customer_order_id),
customer_shipment_id INTEGER(30),
INDEX(customer_shipment_id),
shipped_at DATETIME DEFAULT NULL,
`store_id` INTEGER DEFAULT NULL,
INDEX(store_id),
store_name VARCHAR(255) DEFAULT NULL,
magento_order_number VARCHAR(30) NOT NULL, 
INDEX(magento_order_number), 
status VARCHAR(30) DEFAULT NULL,
INDEX(status),
root_product_id INTEGER NOT NULL, 
INDEX(root_product_id), 
product_id INTEGER NOT NULL, 
INDEX(product_id), 
magento_sale_id INTEGER NOT NULL, 
INDEX(magento_sale_id),
shop VARCHAR(30), 
INDEX(shop),
shop_type VARCHAR(30), 
INDEX(shop_type),
`quantity` decimal(12,4) DEFAULT NULL,
`net_retail` decimal(12,4) DEFAULT NULL COMMENT 'Price to customer, net of VAT and discount',
`net_wholesale` decimal(12,4) DEFAULT NULL,
`net_discount` decimal(12,4) DEFAULT NULL,
`net_shipping_contribution` decimal(12,4) DEFAULT NULL,
  net_shipping_refunded  decimal(12,4) DEFAULT 0,
`average_vat_rate` decimal(12,4) DEFAULT NULL,
`total_vat` decimal(12,4) DEFAULT NULL,
`retail_vat` decimal(12,4) DEFAULT NULL,
`shipping_vat` decimal(12,4) DEFAULT NULL,
`discount_vat` decimal(12,4) DEFAULT NULL,
`qty_invoiced` INT NOT NULL,
`qty_shipped` INT NOT NULL,
  `qty_refunded` INT NOT NULL,
  `qty_canceled` INT NOT NULL,
`refunded_gross_retail` decimal(12,4) DEFAULT NULL,
`refunded_net_retail` decimal(12,4) DEFAULT NULL,
`gross_retail` decimal(12,4) DEFAULT NULL COMMENT 'Price to customer, incl. VAT and net of discount',
gross_discount decimal(12,4) DEFAULT NULL,
`gross_original_retail_value` decimal(12,4) DEFAULT NULL,
`net_original_retail_value` decimal(12,4) DEFAULT NULL,
`gross_shipping_contribution` decimal(12,4) DEFAULT NULL,
`coupon_code` varchar(30) DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT (CASE 
WHEN child_item.child_item_id IS NULL THEN product_orders.item_id
ELSE child_item.child_item_id END
) AS id, 
orders.entity_id AS customer_order_id,
shipment.entity_id AS customer_shipment_id,
shipment.created_at AS shipped_at,
orders.increment_id AS magento_order_number,
orders.created_at AS ordered_at, 
orders.status AS status, 
CASE WHEN qty_canceled != 0 OR qty_refunded != 0 THEN 
	CASE WHEN qty_canceled >= qty_refunded THEN 'canceled' ELSE 'refunded' END
WHEN qty_shipped > 0 THEN 'shipped'
WHEN qty_invoiced > 0 THEN 'invoiced'
WHEN qty_ordered > 0 THEN 'ordered'
ELSE NULL END AS product_status,
root_products.root_product_id,
(CASE 
WHEN child_item.child_item_id IS NULL THEN product_orders.product_id
ELSE child_item.product_id END
) AS product_id,
sale.category_id AS magento_sale_id, 
NULL AS shop,
NULL AS shop_type, -- TO BE SET IN THE DWH
orders.store_id,
NULL AS store_name, -- TO BE SET IN THE DWH
product_orders.qty_ordered AS quantity,
ROUND(product_orders.base_row_total - product_orders.base_discount_amount / (1 + product_orders.tax_percent / 100), 2) AS net_retail,
ROUND(product_orders.qty_ordered * (CASE WHEN mei_wholesale.final_wholesale IS NOT NULL THEN mei_wholesale.final_wholesale ELSE magento_wholesale.value END), 2) AS net_wholesale,
ROUND(product_orders.base_discount_amount / (1 + product_orders.tax_percent / 100), 2) AS net_discount,
ROUND(product_orders.qty_ordered * (orders.base_shipping_amount / orders.total_qty_ordered), 2) AS net_shipping_contribution,
  (CASE WHEN orders.total_refunded = orders.total_qty_ordered THEN ROUND(product_orders.qty_refunded * (orders.base_shipping_amount / orders.total_refunded), 2) ELSE 0 END) AS net_shipping_refunded,
product_orders.tax_percent/100 AS average_vat_rate,
ROUND(product_orders.base_tax_amount 
+ product_orders.qty_ordered * (orders.shipping_tax_amount / orders.total_qty_ordered) 
- product_orders.base_discount_amount * (1 - 1 / (1 + product_orders.tax_percent / 100)), 2) 
AS total_vat,
(product_orders.tax_percent / 100) * (product_orders.base_row_total - product_orders.base_discount_amount / (1 + product_orders.tax_percent / 100)) AS retail_vat,
ROUND(product_orders.qty_ordered * (orders.shipping_tax_amount / orders.total_qty_ordered), 2) AS shipping_vat,
ROUND(product_orders.base_discount_amount * (1 - 1 / (1 + product_orders.tax_percent / 100)), 2) AS discount_vat,
CAST( qty_invoiced AS UNSIGNED ) as qty_invoiced,
CAST( qty_shipped AS UNSIGNED ) as qty_shipped,
  CAST( qty_refunded AS UNSIGNED ) as qty_refunded,
  CAST( qty_canceled AS UNSIGNED ) as qty_canceled,
product_orders.amount_refunded + product_orders.tax_refunded as refunded_gross_retail,
product_orders.amount_refunded as refunded_net_retail,
product_orders.base_row_total_incl_tax - product_orders.base_discount_amount AS gross_retail,
product_orders.base_discount_amount AS gross_discount,
(product_orders.qty_ordered * COALESCE(original_retail.value, original_retail_old.value)) AS gross_original_retail_value,
(product_orders.qty_ordered * COALESCE(original_retail.value, original_retail_old.value)) / (1 + product_orders.tax_percent / 100) AS net_original_retail_value,
product_orders.qty_ordered * (orders.shipping_incl_tax / orders.total_qty_ordered) AS gross_shipping_contribution,
orders.coupon_code

FROM monoqib2c.sales_flat_order_item product_orders
LEFT JOIN monoqib2c.sales_flat_shipment_item shipment_item ON shipment_item.order_item_id = product_orders.item_id
LEFT JOIN monoqib2c.sales_flat_shipment shipment ON shipment_item.parent_id = shipment.entity_id
LEFT JOIN monoqib2c.sales_flat_order orders ON(orders.entity_id = product_orders.order_id)
LEFT JOIN monoqib2c.catalog_product_entity products ON(products.entity_id = product_orders.product_id)
LEFT JOIN warehouse_dev.root_product root_products ON(products.entity_id = root_products.entity_id)
LEFT JOIN monoqib2c.catalog_category_product sale ON(sale.product_id = root_products.root_product_id)
LEFT JOIN monoqib2c.catalog_product_entity_decimal original_retail ON(original_retail.entity_id = root_products.root_product_id AND original_retail.attribute_id = 69 AND original_retail.store_id = orders.store_id)
LEFT JOIN monoqib2c.catalog_product_entity_decimal original_retail_old ON(original_retail_old.entity_id = root_products.root_product_id AND original_retail_old.attribute_id = 69 AND original_retail_old.store_id = 0) -- ORIGINAL RETAIL VALUE -> ONE BY STORE SINCE SWISS SHOP, STORE_ID = 0 BEFORE
LEFT JOIN warehouse_dev.20140127_wholesale_prices_fix_final mei_wholesale ON(mei_wholesale.final_sku = products.sku)
LEFT JOIN monoqib2c.catalog_product_entity_decimal magento_wholesale ON(magento_wholesale.entity_id = root_products.root_product_id AND magento_wholesale.attribute_id = 236 AND magento_wholesale.store_id = 0)
LEFT JOIN warehouse_dev.temp_flat_item_child_id child_item ON(child_item.item_id = product_orders.item_id)

WHERE product_orders.parent_item_id IS NULL
GROUP BY id;

DROP TABLE IF EXISTS warehouse_dev.customer_order_status_history;

CREATE TABLE warehouse_dev.customer_order_status_history AS
SELECT entity_id AS history_id, parent_id AS customer_order_id, status, created_at, comment FROM monoqib2c.sales_flat_order_status_history
WHERE entity_name = 'order';


-- DROP TEMPORARY TABLES

DROP TABLE warehouse_dev.root_product;
DROP TABLE warehouse_dev.temp_flat_item_child_id;
DROP TABLE warehouse_dev.temp_invoice;

DROP TABLE IF EXISTS warehouse_dev.customer_product_order_returns;

CREATE TABLE IF NOT EXISTS warehouse_dev.customer_product_order_returns
(id INTEGER(10) NOT NULL,
PRIMARY KEY(id),
customer_order_id INTEGER(10) NOT NULL, 
INDEX(customer_order_id),
customer_product_order_id INTEGER(10) NOT NULL,
INDEX(customer_product_order_id),
created_at DATETIME DEFAULT NULL,
updated_at DATETIME DEFAULT NULL,
returned_quantity INTEGER(10),
reason VARCHAR(255),
status VARCHAR(255),
quality VARCHAR(2),
label VARCHAR(50)

) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT return_id AS id, item_id AS customer_product_order_id, order_id AS customer_order_id, 
created_at, updated_at, qty_returned AS returned_quantity, reason, status,
quality, label
FROM monoqib2c.fulfillment_return returns
LEFT JOIN monoqib2c.fulfillment_return_reason reason ON reason.reason_id = returns.reason_id
LEFT JOIN monoqib2c.fulfillment_return_status status ON status.status_id = returns.status_id;

DROP TABLE IF EXISTS warehouse_dev.customer_product_order_returns_history;

CREATE TABLE IF NOT EXISTS warehouse_dev.customer_product_order_returns_history
(
  `history_id` int(10) unsigned NOT NULL DEFAULT '0',
  INDEX(`history_id`),
  `return_id` int(10) unsigned NOT NULL,
  INDEX(`return_id`),
  `status` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  INDEX(`username`),
  `created_at` datetime NOT NULL,
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT history_id, return_id, status, username, created_at, comment
FROM monoqib2c.fulfillment_return_history h
LEFT JOIN monoqib2c.fulfillment_return_status s ON s.status_id = h.status_id
LEFT JOIN monoqib2c.admin_user u ON u.user_id = h.admin_user_id;


DROP TABLE IF EXISTS warehouse_dev.customer_shipments;

CREATE TABLE IF NOT EXISTS warehouse_dev.customer_shipments
(id INTEGER(10) NOT NULL,
PRIMARY KEY(id),
customer_order_id INTEGER(10) NOT NULL, 
INDEX(customer_order_id),
customer_id INTEGER(10) NOT NULL, 
INDEX(customer_id), 
created_at DATETIME DEFAULT NULL,
updated_at DATETIME DEFAULT NULL,
total_qty INTEGER(10),

shipping_name VARCHAR(255),
shipping_first_name VARCHAR(255),
shipping_last_name VARCHAR(255),
shipping_company VARCHAR(255),
shipping_address VARCHAR(255),
shipping_city VARCHAR(255),
shipping_post_code VARCHAR(255),
shipping_country VARCHAR(255),
shipping_telephone VARCHAR(255),
shipping_email VARCHAR(255),
email_sent INTEGER(2),
track_number TEXT
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT sfs.entity_id AS id, sfs.order_id AS customer_order_id, sfs.created_at, sfs.updated_at, 
sfs.total_qty, 
CONCAT(shipping.firstname, ' ', shipping.lastname) AS shipping_name,
shipping.firstname AS shipping_first_name,
shipping.lastname AS shipping_last_name,
shipping.company AS shipping_company,
shipping.street AS shipping_address,
shipping.city AS shipping_city,
shipping.postcode AS shipping_post_code,
shipping.country_id AS shipping_country,
shipping.telephone AS shipping_telephone,
shipping.email AS shipping_email,
sfs.email_sent,
track.track_number

FROM monoqib2c.sales_flat_shipment sfs 
LEFT JOIN monoqib2c.sales_flat_order_address shipping ON(shipping.parent_id = sfs.order_id AND shipping.address_type = 'shipping')
LEFT JOIN monoqib2c.sales_flat_shipment_grid grid ON (grid.entity_id = sfs.entity_id)
LEFT JOIN monoqib2c.sales_flat_shipment_track track ON (track.parent_id = sfs.entity_id)
GROUP BY sfs.entity_id;


DROP TABLE IF EXISTS warehouse_dev.credit_memos;
CREATE TABLE IF NOT EXISTS warehouse_dev.credit_memos
(id INTEGER(10) NOT NULL,
PRIMARY KEY(id),
customer_order_id INTEGER(10) NOT NULL, 
INDEX(customer_order_id),
customer_id INTEGER(10) NOT NULL, 
INDEX(customer_id), 
created_at DATETIME DEFAULT NULL,
updated_at DATETIME DEFAULT NULL,
refund_value decimal(12,4) DEFAULT NULL,
status VARCHAR(255) DEFAULT NULL,
creditmemo_reason VARCHAR(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT entity_id AS id, order_id AS customer_order_id, created_at, updated_at, grand_total AS refund_value, 
CASE WHEN state = 2 THEN 'refunded' ELSE 'to_refund' END AS status,
creditmemo_reason
FROM monoqib2c.sales_flat_creditmemo;

DROP TABLE IF EXISTS warehouse_dev.credit_memo_products;
CREATE TABLE IF NOT EXISTS warehouse_dev.credit_memo_products
(id INTEGER(10) NOT NULL,
PRIMARY KEY(id),
credit_memo_id INTEGER(10) NOT NULL, 
INDEX(credit_memo_id),
customer_product_order_id INTEGER(10) NOT NULL, 
INDEX(customer_product_order_id),
product_id INTEGER(10) NOT NULL, 
INDEX(product_id), 
sku VARCHAR(25) DEFAULT NULL,
INDEX(sku),
name VARCHAR(255) DEFAULT NULL,
retail_price decimal(12,4) DEFAULT NULL,
gross_retail decimal(12,4) DEFAULT NULL,
qty INTEGER(10) DEFAULT NULL,
  refunded_amount decimal(12,4) DEFAULT NULL,
tax_amount decimal(12,4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT entity_id AS id, parent_id AS credit_memo_id, order_item_id AS customer_product_order_id, 
sku, name, price_incl_tax AS retail_price,
qty AS quantity, row_total_incl_tax AS gross_retail,
  base_price_incl_tax as refunded_amount,
tax_amount
FROM monoqib2c.sales_flat_creditmemo_item;
