DROP TABLE IF EXISTS warehouse_dev.hellmann_api_responses;

CREATE TABLE IF NOT EXISTS warehouse_dev.hellmann_api_responses (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`orderId` INT(10) NOT NULL,
	`incrementalId` VARCHAR(70) NOT NULL,
	`items` VARCHAR(1000) NULL DEFAULT NULL,
	`transaction_token` VARCHAR(70) NULL DEFAULT NULL COMMENT 'Response token from Hellmann',
	`response_status` VARCHAR(50) NULL DEFAULT NULL COMMENT 'The response status from hellmann',
	`send_date` DATETIME NOT NULL COMMENT 'Datetime when we send the order',
	`response_date` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The response time from hellmann',
	`response_message` VARCHAR(200) NULL DEFAULT NULL COMMENT 'Message from hellmann',
	INDEX `Index_hellmannapi` (`id`),
	INDEX `send_date` (`send_date`, `orderId`)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB
AUTO_INCREMENT=53588

SELECT * FROM monoqib2c.monoqi_hellmannapi_transactions;

