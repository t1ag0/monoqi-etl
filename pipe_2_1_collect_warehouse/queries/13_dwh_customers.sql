CREATE TABLE IF NOT EXISTS warehouse_dev.temp_users_invites

(
`user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
PRIMARY KEY(`user_id`),
INDEX(`user_id`)

) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT value AS user_id, COUNT(DISTINCT entity_id) AS invites FROM monoqib2c.customer_entity_int WHERE value > 9999 AND attribute_id = 186 GROUP BY user_id;

DROP TABLE IF EXISTS warehouse_dev.customers;

CREATE TABLE IF NOT EXISTS warehouse_dev.customers (
`id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
PRIMARY KEY(`id`),
INDEX(`id`),
`email` varchar(255) DEFAULT NULL COMMENT 'Email',
`signup_at` datetime DEFAULT NULL,
`confirmed_at` datetime DEFAULT NULL,
`signup_method` VARCHAR(4) DEFAULT NULL,
`store_id` INTEGER DEFAULT NULL,
INDEX(store_id),
store_name VARCHAR(255) DEFAULT NULL,
INDEX(`store_name`),
`store_language` varchar(2) DEFAULT NULL COMMENT 'Value',
INDEX(`store_language`),
`store_country` varchar(2) DEFAULT NULL COMMENT 'Value',
INDEX(`store_country`),
`gender` varchar(30) DEFAULT NULL,
`age` int(3) DEFAULT NULL,
`last_seen_at` datetime DEFAULT NULL COMMENT 'Value',
`city` varchar(255) DEFAULT NULL,
`country` varchar(255) DEFAULT NULL,
`postal_code` varchar(255) DEFAULT NULL,
`telephone` varchar(30) DEFAULT NULL,
uuid varchar(255) DEFAULT NULL,
INDEX(uuid),
`source_id` int(11) DEFAULT '0' COMMENT 'Value',
`source_channel` varchar(30) DEFAULT NULL,
`source_name` varchar(30) DEFAULT NULL,
`source_country` varchar(30) DEFAULT NULL,
`utm_campaign` varchar(255) DEFAULT NULL COMMENT 'Value',
`utm_medium` varchar(255) DEFAULT NULL COMMENT 'Value',
`utm_source` varchar(255) DEFAULT NULL COMMENT 'Value',
`utm_content` varchar(255) DEFAULT NULL COMMENT 'Value',
`utm_term` varchar(255) DEFAULT NULL COMMENT 'Value',
invites INTEGER(10) NOT NULL,

INDEX(`email`),
INDEX(`source_id`),
INDEX(`source_channel`),
INDEX(`source_country`),
INDEX(`source_name`),
INDEX(`utm_campaign`),
INDEX(`utm_medium`),
INDEX(`utm_source`),
INDEX(`utm_content`),
INDEX(`utm_term`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT customer.entity_id AS id, 
customer.email,  
(CASE WHEN customer.created_at <= '2014-02-17' THEN signup_old_fix.signup_at ELSE customer.created_at END) AS signup_at,
confirmed_at2.value AS confirmed_at,
(CASE WHEN customer.fbid IS NOT NULL THEN 'fb' ELSE 'mail' END) AS signup_method,
customer.store_id AS store_id,
NULL AS store_name,
NULL AS store_language,
NULL AS store_country,
CASE WHEN gender2.value = 1 THEN 'M' WHEN gender2.value = 2 THEN 'F' ELSE NULL END AS gender,
TIMESTAMPDIFF(YEAR, age.value, CURRENT_DATE) AS age,

IFNULL(last_seen.value, last_seen2.value) AS last_seen_at,


(CASE WHEN
city.value IS NOT NULL THEN city.value
WHEN address2.city IS NOT NULL THEN address2.city
WHEN address3.city IS NOT NULL THEN address3.city
WHEN
SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( fb_data.value , 's:8:"location"', -1 ), ';', 5),';',-1), '"',2),'"',-1),', ',1)
!=
SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( fb_data.value , 's:8:"location"', -1 ), ';', 5),';',-1), '"',2),'"',-1),', ',-1) 
THEN
SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( fb_data.value , 's:8:"location"', -1 ), ';', 5),';',-1), '"',2),'"',-1),', ',1)
END)
AS city, 

(CASE WHEN 
country.value IS NOT NULL THEN country.value
WHEN address2.country_id IS NOT NULL THEN address2.country_id
WHEN address3.country_id IS NOT NULL THEN address3.country_id
WHEN

SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( fb_data.value , 's:8:"location"', -1 ), ';', 5),';',-1), '"',2),'"',-1),', ',-1) 
!=
SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( fb_data.value , 's:8:"location"', -1 ), ';', 5),';',-1), '"',2),'"',-1),', ',1)
THEN
SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX(SUBSTRING_INDEX( fb_data.value , 's:8:"location"', -1 ), ';', 5),';',-1), '"',2),'"',-1),', ',-1)
END) AS country,
postal_code.value AS postal_code,
telephone.value AS telephone,

uuid.value AS uuid,

IFNULL(source.value, IFNULL(inviter_id.value, source2.value)) AS source_id, 
-- inviter_id table is a patch for the registration referral ticket MON-147, should be modified back when ticket is closed
'Other' AS source_channel,
'Other' AS source_name, -- DEFAULT TO OTHER - OVERWRITTEN IN STATIC_ATTRIBUTES
source_name.source_country AS source_country,
utm_campaign.value AS utm_campaign,
utm_medium.value AS utm_medium,
utm_source.value AS utm_source,
utm_content.value AS utm_content,
utm_term.value AS utm_term,
invites.invites


FROM monoqib2c.customer_entity customer
LEFT JOIN warehouse_dev.customer_signups signup_old_fix ON(customer.entity_id = signup_old_fix.user_id)
LEFT JOIN monoqib2c.customer_entity_datetime confirmed_at2 ON (confirmed_at2.entity_id = customer.entity_id AND confirmed_at2.attribute_id = 302)
LEFT JOIN monoqib2c.customer_entity_varchar last_seen ON(last_seen.entity_id = customer.entity_id AND last_seen.attribute_id = 191)
LEFT JOIN monoqib2c.customer_entity_datetime last_seen2 ON(last_seen2.entity_id = customer.entity_id AND last_seen2.attribute_id = 191)
LEFT JOIN monoqib2c.customer_entity_int source ON(source.entity_id = customer.entity_id AND source.attribute_id = 186)
LEFT JOIN monoqib2c.customer_entity_varchar source2 ON (source2.entity_id = customer.entity_id AND source2.attribute_id = 312)
LEFT JOIN warehouse_dev.marketing_codes AS source_name ON (source_name.source_id = COALESCE(IFNULL(source.value, source2.value), 0))
LEFT JOIN monoqib2c.customer_entity_varchar inviter_id ON (customer.entity_id = inviter_id.entity_id AND inviter_id.attribute_id = 299 AND inviter_id.value REGEXP '^[0-9]{5,}$')
LEFT JOIN monoqib2c.customer_entity_int AS gender2 ON ( gender2.entity_id = customer.entity_id AND gender2.attribute_id = 18)
LEFT JOIN monoqib2c.customer_entity_datetime age ON (age.entity_id = customer.entity_id AND age.attribute_id = 11)
LEFT JOIN monoqib2c.customer_entity_text fb_data ON (fb_data.entity_id = customer.entity_id AND fb_data.attribute_id = 193)
LEFT JOIN monoqib2c.customer_entity_varchar utm_campaign ON(customer.entity_id = utm_campaign.entity_id AND utm_campaign.attribute_id = 227)
LEFT JOIN monoqib2c.customer_entity_varchar utm_medium ON(customer.entity_id = utm_medium.entity_id AND utm_medium.attribute_id = 228)
LEFT JOIN monoqib2c.customer_entity_varchar utm_source ON(customer.entity_id = utm_source.entity_id AND utm_source.attribute_id = 229)
LEFT JOIN monoqib2c.customer_entity_varchar utm_content ON(customer.entity_id = utm_content.entity_id AND utm_content.attribute_id = 230)
LEFT JOIN monoqib2c.customer_entity_varchar utm_term ON(customer.entity_id = utm_term.entity_id AND utm_term.attribute_id = 288)
LEFT JOIN monoqib2c.customer_entity_datetime signup_at ON(signup_at.entity_id = customer.entity_id AND signup_at.attribute_id = 284)
LEFT JOIN warehouse_dev.customer_address_id address_id ON(address_id.entity_id = customer.entity_id)
LEFT JOIN warehouse_dev.customer_address_2 address2 ON(address2.entity_id = customer.entity_id)
LEFT JOIN warehouse_dev.customer_address_3 address3 ON(address3.entity_id = customer.entity_id)
LEFT JOIN monoqib2c.customer_address_entity_varchar city ON(city.entity_id = address_id.address_id AND city.attribute_id = 26)
LEFT JOIN monoqib2c.customer_address_entity_varchar country ON(city.entity_id = country.entity_id AND country.attribute_id = 27)
LEFT JOIN monoqib2c.customer_address_entity_varchar postal_code ON(city.entity_id = postal_code.entity_id AND postal_code.attribute_id = 30)
LEFT JOIN monoqib2c.customer_address_entity_varchar telephone ON(city.entity_id = telephone.entity_id AND telephone.attribute_id = 31)
LEFT JOIN monoqib2c.customer_entity_varchar uuid ON (uuid.entity_id = customer.entity_id AND uuid.attribute_id = 321)
LEFT JOIN warehouse_dev.temp_users_invites invites ON(invites.user_id = customer.entity_id)

WHERE customer.entity_id > 9999

GROUP BY customer.entity_id;

DROP TABLE IF EXISTS warehouse_dev.customer_address_id;
DROP TABLE IF EXISTS warehouse_dev.customer_address_2;
DROP TABLE IF EXISTS warehouse_dev.customer_address_3;
DROP TABLE IF EXISTS warehouse_dev.temp_users_invites;

DROP TABLE IF EXISTS warehouse_dev.newsletter_subscribers;

CREATE TABLE IF NOT EXISTS warehouse_dev.newsletter_subscribers (
`customer_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Entity Id',
PRIMARY KEY(`customer_id`),
INDEX(`customer_id`),
`email` varchar(255) DEFAULT NULL COMMENT 'Email',
INDEX(`email`),
`store_id` int(3),
INDEX(`store_id`),
`language` varchar(10) DEFAULT NULL COMMENT 'Value',
`country` varchar(10) DEFAULT NULL COMMENT 'Value',
latest_updated_at TIMESTAMP,
`newsletter_daily` INTEGER(2) DEFAULT NULL,
INDEX(`newsletter_daily`),
`newsletter_weekly` INTEGER(2) DEFAULT NULL,
INDEX(`newsletter_weekly`),
`newsletter_special` INTEGER(2) DEFAULT NULL,
INDEX(`newsletter_special`),
`autologin_token` char(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8


SELECT ce.entity_id AS customer_id, 
ce.email,
CASE WHEN newsletter_subscriber.store_id > 0 THEN newsletter_subscriber.store_id ELSE ce.store_id END AS store_id,
CASE WHEN newsletter_subscriber.language > 0 THEN 
	CASE WHEN newsletter_subscriber.language IN (1,5) THEN 'de'
	WHEN newsletter_subscriber.language IN (2,6,7) THEN 'en'
	ELSE NULL END
ELSE NULL END AS language, 
NULL AS country, 
ce.autologin_token, 
MAX(IFNULL(change_status_at, '2014-02-17')) AS latest_updated_at,
CASE WHEN confirmed_at.value IS NULL AND (confirmed_at2.value IS NULL OR confirmed_at2.value = 0) -- unconfirmed
OR ce.autologin_token IS NULL -- no login key
THEN 0 
ELSE MAX(CASE WHEN type_id = 1 AND subscriber_status = 1 THEN 1 ELSE 0 END) END AS newsletter_daily, 
CASE WHEN (COUNT(CASE WHEN type_id = 1 AND subscriber_status = 1 THEN 1 ELSE NULL END) > 0 AND MAX(change_status_at) IS NULL) -- before relaunch people where signed up by default to both NLs
OR (confirmed_at.value IS NULL AND (confirmed_at2.value IS NULL OR confirmed_at2.value = 0)) -- unconfirmed
OR ce.autologin_token IS NULL -- no login key
THEN 0 
ELSE MAX(CASE WHEN type_id = 2 AND subscriber_status = 1 THEN 1 ELSE 0 END) END AS newsletter_weekly,
CASE WHEN confirmed_at.value IS NULL AND (confirmed_at2.value IS NULL OR confirmed_at2.value = 0) -- unconfirmed
OR ce.autologin_token IS NULL -- no login key
THEN 0
ELSE MAX(CASE WHEN type_id = 4 AND subscriber_status = 1 THEN 1 ELSE 0 END) END AS newsletter_special

FROM monoqib2c.newsletter_subscriber
	RIGHT JOIN monoqib2c.customer_entity ce ON ce.entity_id = customer_id
	LEFT JOIN monoqib2c.customer_entity_varchar confirmed_at ON confirmed_at.entity_id = ce.entity_id AND confirmed_at.attribute_id = 178
	LEFT JOIN monoqib2c.customer_entity_datetime confirmed_at2 ON confirmed_at2.entity_id = ce.entity_id AND confirmed_at2.attribute_id = 302

WHERE ce.entity_id > 9999 AND (confirmed_at.value IS NOT NULL OR (confirmed_at2.value IS NOT NULL AND confirmed_at2.value != 0))

GROUP BY ce.entity_id;