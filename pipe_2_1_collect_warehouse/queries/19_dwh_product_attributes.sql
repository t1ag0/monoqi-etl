
DROP TABLE IF EXISTS warehouse_dev.product_attributes;
DROP TABLE IF EXISTS warehouse_dev.root_product;

CREATE TABLE warehouse_dev.root_product 
(entity_id INTEGER NOT NULL, 
PRIMARY KEY(entity_id), 
INDEX(entity_id), 
root_product INTEGER NOT NULL, 
INDEX(root_product)) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT product.entity_id, (CASE WHEN relation.child_id IS NULL THEN product.entity_id ELSE relation.parent_id END) AS root_product
FROM monoqib2c.catalog_product_entity product
LEFT JOIN monoqib2c.catalog_product_relation relation ON(relation.child_id = product.entity_id)

GROUP BY entity_id;



CREATE TABLE warehouse_dev.product_attributes 
(product_id INTEGER NOT NULL, 
PRIMARY KEY(product_id),
en_name VARCHAR(255),
de_name VARCHAR(255),
en_brand_name VARCHAR(255),
de_brand_name VARCHAR(255),
en_country_of_origin VARCHAR(255),
de_country_of_origin VARCHAR(255),
en_description TEXT(2000),
de_description TEXT(2000),
en_material VARCHAR(255),
de_material VARCHAR(255),
en_color VARCHAR(255),
de_color VARCHAR(255),
en_measurements VARCHAR(255),
de_measurements VARCHAR(255),
en_weight VARCHAR(255),
de_weight VARCHAR(255),
en_comment VARCHAR(255),
de_comment VARCHAR(255),
en_care VARCHAR(255),
de_care VARCHAR(255),
en_tech_specs VARCHAR(255),
de_tech_specs VARCHAR(255),
en_shipping_comment VARCHAR(255),
de_shipping_comment VARCHAR(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT product.entity_id AS product_id,
(CASE WHEN en_name.value IS NOT NULL THEN en_name.value ELSE default_name.value END) AS en_name,
(CASE WHEN de_name.value IS NOT NULL THEN de_name.value ELSE default_name.value END) AS de_name,
(CASE WHEN en_brand_name.value IS NOT NULL THEN en_brand_name.value ELSE default_brand_name.value END) AS en_brand_name,
(CASE WHEN de_brand_name.value IS NOT NULL THEN de_brand_name.value ELSE default_brand_name.value END) AS de_brand_name,
(CASE WHEN en_country_of_origin.value IS NOT NULL THEN en_country_of_origin.value ELSE default_country_of_origin.value END) AS en_country_of_origin,
(CASE WHEN de_country_of_origin.value IS NOT NULL THEN de_country_of_origin.value ELSE default_country_of_origin.value END) AS de_country_of_origin,
(CASE WHEN en_description.value IS NOT NULL THEN en_description.value ELSE default_description.value END) AS en_description,
(CASE WHEN de_description.value IS NOT NULL THEN de_description.value ELSE default_description.value END) AS de_description,
(CASE WHEN en_material.value IS NOT NULL THEN en_material.value ELSE default_material.value END) AS en_material,
(CASE WHEN de_material.value IS NOT NULL THEN de_material.value ELSE default_material.value END) AS de_material,
(CASE WHEN en_color.value IS NOT NULL THEN en_color.value ELSE default_color.value END) AS en_color,
(CASE WHEN de_color.value IS NOT NULL THEN de_color.value ELSE default_color.value END) AS de_color,
(CASE WHEN en_measurements.value IS NOT NULL THEN en_measurements.value ELSE default_measurements.value END) AS en_measurements,
(CASE WHEN de_measurements.value IS NOT NULL THEN de_measurements.value ELSE default_measurements.value END) AS de_measurements,
(CASE WHEN en_weight.value IS NOT NULL THEN en_weight.value ELSE default_weight.value END) AS en_weight,
(CASE WHEN de_weight.value IS NOT NULL THEN de_weight.value ELSE default_weight.value END) AS de_weight,
(CASE WHEN en_comment.value IS NOT NULL THEN en_comment.value ELSE default_comment.value END) AS en_comment,
(CASE WHEN de_comment.value IS NOT NULL THEN de_comment.value ELSE default_comment.value END) AS de_comment,
(CASE WHEN en_care.value IS NOT NULL THEN en_care.value ELSE default_care.value END) AS en_care,
(CASE WHEN de_care.value IS NOT NULL THEN de_care.value ELSE default_care.value END) AS de_care,
(CASE WHEN en_tech_specs.value IS NOT NULL THEN en_tech_specs.value ELSE default_tech_specs.value END) AS en_tech_specs,
(CASE WHEN de_tech_specs.value IS NOT NULL THEN de_tech_specs.value ELSE default_tech_specs.value END) AS de_tech_specs,
(CASE WHEN en_shipping_comment.value IS NOT NULL THEN en_shipping_comment.value ELSE default_shipping_comment.value END) AS en_shipping_comment,
(CASE WHEN de_shipping_comment.value IS NOT NULL THEN de_shipping_comment.value ELSE default_shipping_comment.value END) AS de_shipping_comment

FROM monoqib2c.catalog_product_entity product
LEFT JOIN warehouse_dev.root_product root_product ON(root_product.entity_id = product.entity_id)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_name ON(en_name.entity_id = root_product.root_product AND en_name.attribute_id = 65 AND en_name.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_name ON(de_name.entity_id = root_product.root_product AND de_name.attribute_id = 65 AND de_name.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_name ON(default_name.entity_id = root_product.root_product AND default_name.attribute_id = 65 AND default_name.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_brand_name ON(de_brand_name.entity_id = root_product.root_product AND de_brand_name.attribute_id = 202 AND de_brand_name.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_brand_name ON(en_brand_name.entity_id = root_product.root_product AND en_brand_name.attribute_id = 202 AND en_brand_name.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_brand_name ON(default_brand_name.entity_id = root_product.root_product AND default_brand_name.attribute_id = 202 AND default_brand_name.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_material ON(de_material.entity_id = root_product.root_product AND de_material.attribute_id = 203 AND de_material.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_material ON(en_material.entity_id = root_product.root_product AND en_material.attribute_id = 203 AND en_material.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_material ON(default_material.entity_id = root_product.root_product AND default_material.attribute_id = 203 AND default_material.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_color ON(de_color.entity_id = root_product.root_product AND de_color.attribute_id = 204 AND de_color.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_color ON(en_color.entity_id = root_product.root_product AND en_color.attribute_id = 204 AND en_color.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_color ON(default_color.entity_id = root_product.root_product AND default_color.attribute_id = 204 AND default_color.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_measurements ON(de_measurements.entity_id = root_product.root_product AND de_measurements.attribute_id = 205 AND de_measurements.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_measurements ON(en_measurements.entity_id = root_product.root_product AND en_measurements.attribute_id = 205 AND en_measurements.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_measurements ON(default_measurements.entity_id = root_product.root_product AND default_measurements.attribute_id = 205 AND default_measurements.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_country_of_origin ON(de_country_of_origin.entity_id = root_product.root_product AND de_country_of_origin.attribute_id = 206 AND de_country_of_origin.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_country_of_origin ON(en_country_of_origin.entity_id = root_product.root_product AND en_country_of_origin.attribute_id = 206 AND en_country_of_origin.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_country_of_origin ON(default_country_of_origin.entity_id = root_product.root_product AND default_country_of_origin.attribute_id = 206 AND default_country_of_origin.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_care ON(de_care.entity_id = root_product.root_product AND de_care.attribute_id = 207 AND de_care.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_care ON(en_care.entity_id = root_product.root_product AND en_care.attribute_id = 207 AND en_care.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_care ON(default_care.entity_id = root_product.root_product AND default_care.attribute_id = 207 AND default_care.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_comment ON(de_comment.entity_id = root_product.root_product AND de_comment.attribute_id = 213 AND de_comment.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_comment ON(en_comment.entity_id = root_product.root_product AND en_comment.attribute_id = 213 AND en_comment.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_comment ON(default_comment.entity_id = root_product.root_product AND default_comment.attribute_id = 213 AND default_comment.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_weight ON(de_weight.entity_id = root_product.root_product AND de_weight.attribute_id = 225 AND de_weight.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_weight ON(en_weight.entity_id = root_product.root_product AND en_weight.attribute_id = 225 AND en_weight.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_weight ON(default_weight.entity_id = root_product.root_product AND default_weight.attribute_id = 225 AND default_weight.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_shipping_comment ON(de_shipping_comment.entity_id = root_product.root_product AND de_shipping_comment.attribute_id = 265 AND de_shipping_comment.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_shipping_comment ON(en_shipping_comment.entity_id = root_product.root_product AND en_shipping_comment.attribute_id = 265 AND en_shipping_comment.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_shipping_comment ON(default_shipping_comment.entity_id = root_product.root_product AND default_shipping_comment.attribute_id = 265 AND default_shipping_comment.store_id = 0)
LEFT JOIN monoqib2c.catalog_product_entity_varchar de_tech_specs ON(de_tech_specs.entity_id = root_product.root_product AND de_tech_specs.attribute_id = 267 AND de_tech_specs.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_varchar en_tech_specs ON(en_tech_specs.entity_id = root_product.root_product AND en_tech_specs.attribute_id = 267 AND en_tech_specs.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_varchar default_tech_specs ON(default_tech_specs.entity_id = root_product.root_product AND default_tech_specs.attribute_id = 267 AND default_tech_specs.store_id = 0)

LEFT JOIN monoqib2c.catalog_product_entity_text de_description ON(de_description.entity_id = root_product.root_product AND de_description.attribute_id = 67 AND de_description.store_id = 1)
LEFT JOIN monoqib2c.catalog_product_entity_text en_description ON(en_description.entity_id = root_product.root_product AND en_description.attribute_id = 67 AND en_description.store_id = 2)
LEFT JOIN monoqib2c.catalog_product_entity_text default_description ON(default_description.entity_id = root_product.root_product AND default_description.attribute_id = 67 AND default_description.store_id = 0)

WHERE product.sku IS NOT NULL

GROUP BY product_id;

DROP TABLE warehouse_dev.root_product;

DROP TABLE IF EXISTS warehouse_dev.product_picture_urls;

CREATE TABLE IF NOT EXISTS warehouse_dev.product_picture_urls 
(product_id INTEGER NOT NULL, 
PRIMARY KEY(product_id),
thumbnail_url VARCHAR(255),
base_url VARCHAR(255),
d1_url VARCHAR(255),
d2_url VARCHAR(255),
d3_url VARCHAR(255)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT tb.entity_id AS product_id, tb.value AS thumbnail_url, b.value AS base_url, d1.value AS d1_url, d2.value AS d2_url, d3.value AS d3_url 
FROM monoqib2c.catalog_product_entity_varchar tb
LEFT JOIN monoqib2c.catalog_product_entity_varchar b ON b.entity_id = tb.entity_id AND b.attribute_id = 79 AND b.store_id = 0
LEFT JOIN monoqib2c.catalog_product_entity_varchar d1 ON d1.entity_id = tb.entity_id AND d1.attribute_id = 199 AND d1.store_id = 0
LEFT JOIN monoqib2c.catalog_product_entity_varchar d2 ON d2.entity_id = tb.entity_id AND d2.attribute_id = 200 AND d2.store_id = 0
LEFT JOIN monoqib2c.catalog_product_entity_varchar d3 ON d3.entity_id = tb.entity_id AND d3.attribute_id = 201 AND d3.store_id = 0
WHERE tb.attribute_id = 80 AND tb.store_id = 0
GROUP BY tb.entity_id;