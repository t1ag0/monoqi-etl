
DROP TABLE IF EXISTS warehouse_dev.invites;

CREATE TABLE warehouse_dev.invites(
id INT(10),
created_at DATETIME,
INDEX(created_at),
accepted_at DATETIME,
INDEX(accepted_at),
rewarded_at DATETIME,
INDEX(rewarded_at),
sender_id INT(10),
INDEX(sender_id),
receiver_id INT(10),
INDEX(receiver_id),
status INT(10),
INDEX(status),
invite_channel VARCHAR(30),
INDEX(invite_channel)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT * FROM

((SELECT id, 
create_at AS created_at,
(CASE WHEN invite_state BETWEEN 20 AND 49 THEN update_at ELSE NULL END) AS accepted_at,
IFNULL(used_at, (CASE WHEN invite_state >= 50 THEN update_at ELSE NULL END)) AS rewarded_at,
i1.user_id AS sender_id,
invited_user_id AS receiver_id,
(CASE WHEN used_at IS NOT NULL THEN invite_state + TRUNCATE((59-invite_state)/10,0)*10 ELSE invite_state END) AS status,
(CASE WHEN invite_type = 1 THEN 'facebook'
WHEN invite_type = 2 THEN 'email'
WHEN invite_type = 3 THEN 'email'
WHEN invite_type = 4 THEN 'email' END) AS invite_channel

FROM monoqib2c.ext_invite i1
LEFT JOIN monoqib2c.ext_invite2 i2 ON i1.user_id = i2.user_id AND i1.invited_user_id = i2.invited

WHERE i1.user_id > 9999)

UNION ALL

(SELECT ci.id,
created_at,
used_at AS accepted_at,
NULL AS rewarded_at,
inviter_id AS sender_id,
ci.customer_id AS receiver_id,
IF(ci.customer_id IS NULL, 
	10, 
	IF(c.confirmed_at IS NULL, 
		IF(co.id IS NULL, 
			25, 
			IF(co.status NOT IN ('processing', 'complete'), 
				40,
				50
				)
			),
		IF(co.id IS NULL,
			30,
			IF(co.status NOT IN ('processing', 'complete'),
				41,
				51
				)
			)
		)
	) AS status,
NULL AS invite_channel

FROM monoqib2c.innobyte_customer_invites ci
LEFT JOIN warehouse_dev.customer_orders co ON co.customer_id = ci.customer_id AND lower(coupon_code) = 'mqwu8gh3i'
LEFT JOIN warehouse_dev.customers c ON c.id = ci.customer_id)

) t1

ORDER BY -created_at DESC;