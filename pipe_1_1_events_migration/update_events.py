#!/usr/bin/env python3
"""
Update product views in DWH from monoqib2c.report_event.
 TODO: move queries to somewhere else (static methd?).
 TODO: simplify and reuse queries.
 TODO: DDL Queries here.
"""

import time
from io import StringIO
import csv
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from monoqi_etl import MonoqiETL, parse_args, timeit

__author__ = 'Tiago Perez <tiago@cavalcanti.name>'

SRC_DB = "magentodb"
TARGET_DB = 'dwh2'
TARGET_TABLE = 'events'

"""
event_type_id	event_name	customer_login
1	catalog_product_view	0
2	sendfriend_product	0
3	catalog_product_compare_add_product	0
4	checkout_cart_add_product	0
5	wishlist_add_product	0
6	wishlist_share	0
"""

CLASS_NAME = 'UpdateEvents'


class UpdateEvents(MonoqiETL):
    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        #
        self.src_db = create_engine(self.conf['src_db'])
        self.target_db = create_engine(self.conf['target_db'])
        #
        self.read_block = self.conf['read_block']
        self.write_block = self.conf['write_block']
        self.from_idx = self.conf['idx0'] if 'idx0' in self.conf else 0
        self.data = None
        self.target_table = TARGET_TABLE
        self.day = kwargs['last_day']

    @staticmethod
    def _query(from_idx, read_block):
        """
        The basic query to retrieve data from report_event.
        :param from_idx: first event_id
        :param read_block: max number of events to query
        :return: the formatted query ready to run.
        """
        """
        The basic query.
        """
        b = StringIO()
        sql = """select
            re.event_id,
            object_id AS product_id,
            subject_id AS visitor_id,
            event_type_id,
            DATE(logged_at) AS event_date,
            HOUR(logged_at) AS event_hour,
            store_id,
            re.logged_at as time_of_visit
        FROM monoqib2c.report_event re
        WHERE event_id > {from_idx}
            and (event_type_id = 1 OR event_type_id = 4)
            AND subtype = 0
        ORDER BY re.event_id ASC
        LIMIT {read_block}
        ;"""
        return sql.format(from_idx=from_idx, read_block=read_block)

    def _get_first_idx(self, from_idx):
        """
        Get the first to query from the magento report events.
        This will be the MAX of the last event on the target db and
        the given from_idx
        :param from_idx: the first event to query from the source db
        :return:
        """
        res = self.target_db.execute("""
            select
            max(event_id) as last_event
            from {schema}.{table} pv
            """.format(table=self.target_table
                       , schema=self.conf['schema']))
        res = res.fetchone()[0]
        res = res if res is not None else 0
        return max(res, from_idx)

    def test_conf(self):
        self.log.info('test connections')
        self.log.info(' SRD DB')
        try:
            print(self.src_db.connect())
        except:
            self.log.warning(" Error connecting to SRC " + self.conf['magentodb'])
        self.log.info(' SRD DB ok ')

        self.log.info(' TARGET DB')
        try:
            print(self.target_db.connect())
        except:
            self.log.warning(" Error connecting to TARGET " + self.conf['dwh2'])
        self.log.info(' TARGET DB ok')

    def extract(self):
        self.log.info(' Extract ')
        # get first id to download
        _first_id = self._get_first_idx(self.from_idx)
        _sql = UpdateEvents._query(_first_id, self.read_block)
        t0 = time.clock()
        df = pd.read_sql(_sql, self.src_db, index_col="event_id")
        t1 = time.clock()
        msg = " Extract: \n TIME READ  : " + str(round(t1-t0, 4))
        msg += '\n ROWS       : ' + str(len(df))
        self.log.info(msg)
        self.data = df

    def load(self):
        if self.data.empty:
            self.log.info(" Target table up to date. Nothing more to update")
            return -1
        self.log.info(' Load ')
        from_idx = self.from_idx
        first_idx = self.data.index.min()
        last_idx = self.data.index.values.max()
        min_date = self.data.event_date.min()
        max_date = self.data.event_date.max()
        t0 = time.clock()
        self._load_pg_copy()
        t1 = time.clock()
        msg = " Load \n IDX0: " + str(from_idx) + " BLOCK: " + str(self.read_block)
        msg += "\n First: " + str(first_idx) + " Last : " + str(last_idx)
        msg += "\n First Date: " + str(min_date) + " Last Date: " + str(max_date)
        msg += " \n TIME WRITE  : "+str(round(t1-t0, 4))
        self.log.info(msg)
        if max_date >= self.day:
            self.log.info(" Target table up to date.")
            last_idx = -1
        return last_idx

    def _load_direct_write(self):
        self.data.to_sql(self.target_table
                         , self.target_db
                         , schema=self.conf['schema']
                         , if_exists='append'
                         , index=True
                         , index_label="event_id"
                         , chunksize=self.write_block)

    def _load_pg_copy(self):
        csv_buff = StringIO()
        self.data.to_csv(csv_buff, quoting=csv.QUOTE_NONNUMERIC)
        csv_buff.seek(0)
        #
        Session = sessionmaker(bind=self.target_db)
        session = Session()
        cursor = session.connection().connection.cursor()
        copy_sql = 'COPY events.events FROM STDIN WITH CSV HEADER;'
        cursor.copy_expert(copy_sql, csv_buff)
        session.commit()
        cursor.close()

    def test(self):
        """
        Check SRC and TARGET tables have the same elements between first
        and last indices.
        :return: True if OK.
        """
        target_query = """
            select
                pv.event_type_id
                , min(event_id) as first_event
                , max(event_id) as last_event
                , count(event_id) as n_events
            from {schema}.{table} pv
            group by 1
            ORDER BY 1 ASC
            ;""".format(schema=self.conf['schema'], table=self.target_table)
        src_query = """
            SELECT
                event_type_id
                , min(event_id) as first_event
                , max(event_id) as last_event
                , count(event_id) as n_events
            FROM monoqib2c.report_event re
            WHERE event_id BETWEEN {first} AND {last}
                AND subtype = 0
                AND event_type_id in (1, 4)
            GROUP BY 1
            ORDER BY 1 ASC;
            """

        #
        # Read target DB for the firs and last idx, and number of events.
        # grouped by event type
        t0 = time.clock()
        target_df = pd.read_sql(target_query, self.target_db
                                , index_col='event_type_id')
        t1 = time.clock() - t0

        first = target_df.first_event.min()
        last = target_df.last_event.max()

        #
        # Get same info as before from src DB.
        t2 = time.clock()
        src_df = pd.read_sql(src_query.format(first=first, last=last)
                             , self.src_db
                             , index_col='event_type_id')
        t3 = time.clock()-t2

        #
        # Compare
        eq = target_df.equals(src_df)

        debug_msg = ' T Query Target: ' + str(round(t1, 2))
        debug_msg += '\n Target First Last ' + str(first) + " " + str(last)
        debug_msg += '\n T Query Src: ' + str(round(t3, 2))
        debug_msg += '\n Src and Target equal? : ' + str(eq)
        self.log.info(debug_msg)
        return eq, first, last

    @timeit
    def run(self):
        self.log.info(" Update until date : " + str(self.day))
        last = 1
        while last > 0:
            self.extract()
            last = self.load()
        self.log.info("done")

if __name__ == "__main__":
    _kwarg_ = parse_args()
    etl = UpdateEvents(**_kwarg_)
    etl.print_conf()
    etl.run()
    # etl.check_consistency()

