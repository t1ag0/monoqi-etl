#!/usr/bin/env python3
import time
from monoqi_etl import parse_args, setup_logging
from update_events import UpdateEvents
from update_shop_views_day import UpdateShopViewsDay
from update_visitors_product import UpdateVisitorsProduct
from update_unique_visitors import UpdateUniqueVisitors
from update_product_views import UpdateProductViews

__author__ = 'Tiago <tiago@monoqi.de>'


def update_events(**kwargs):
    log_name = "UpdateEvents"
    log = setup_logging(log_name, 'INFO')
    log.info('hello')
    _1_updateEvents = UpdateEvents(**kwargs)
    _2_updateShopViewsDay = UpdateShopViewsDay(**kwargs)
    _3_updateVisitorsProduct = UpdateVisitorsProduct(**kwargs)
    _4_updateUniqueVisitors = UpdateUniqueVisitors(**kwargs)
    _5_updateProductViews = UpdateProductViews(**kwargs)
    #
    log.info(' UPDATE EVENTS START DAY: ' + str(kwargs['day']))
    t0 = time.time()
    _1_updateEvents.print_conf()
    _1_updateEvents.run()
    _2_updateShopViewsDay.run()
    _3_updateVisitorsProduct.run()
    _4_updateUniqueVisitors.run()
    _5_updateProductViews.run()
    t1 = time.time() - t0
    log.info(' UPDATE EVENTS STOP  DAY: ' + str(kwargs['day']))
    log.info(' UPDATE EVENTS TIME     : ' + str(round(t1, 2)) + ' seconds.')

if __name__ == "__main__":
    args = parse_args()
    update_events(**args)
