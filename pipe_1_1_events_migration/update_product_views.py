#!/usr/bin/env python3
"""
Update product views
"""
from datetime import date
from sqlalchemy import create_engine
import pandas as pd
import time

from monoqi_etl import MonoqiETL, parse_args, load_pg_copy, timeit

__author__ = 'Tiago Perez <tiago@cavalcanti.name>'

CLASS_NAME = 'UpdateProductViews'


class UpdateProductViews(MonoqiETL):
    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        #

        #
        self.src_db = create_engine(self.conf['src_db'])
        self.target_db = create_engine(self.conf['target_db']
                                       , pool_reset_on_return='commit')
        self.schema = self.conf['schema']
        #
        self.src_table = self.conf['src_table']
        self.target_tables = self.conf['target_tables']
        self.root_product = self.conf['root_product']
        #
        self.day = self.conf['day']

    @staticmethod
    def get_queries():
        queries = dict()
        queries['products_visited_by_user'] = """
        DELETE FROM {schema}.{target_table} t
            WHERE t.date>='{day}';
        INSERT INTO {schema}.{target_table}
        SELECT
            visitor_id AS customer_id,
            product_id,
            event_date AS date,
            COUNT(event_id) AS times_seen
        FROM {schema}.{src_table}
        WHERE event_date >= '{day}'
            AND event_type_id = 1
        GROUP BY visitor_id, product_id, event_date;
        """

        queries['product_views'] = """
            TRUNCATE TABLE {schema}.{target_table};
            INSERT INTO {schema}.{target_table}
            SELECT
                product_id AS root_product_id,
                COUNT( DISTINCT customer_id ) AS viewers,
                SUM(times_seen) AS views
            FROM {schema}.products_visited_by_user
                WHERE product_id IN (
                    SELECT DISTINCT root_product_id
                    FROM events.root_product
                    WHERE root_product_id IS NOT NULL
                    )
            GROUP BY product_id;
        """

        queries['product_day_views'] = """
            DELETE FROM {schema}.{target_table} t
                WHERE t.day>='{day}';
            INSERT INTO {schema}.{target_table}
            SELECT
                product_id AS root_product_id,
                date AS day,
                COUNT( DISTINCT customer_id ) AS viewers,
                SUM(times_seen) AS views
            FROM {schema}.products_visited_by_user
            WHERE date >='{day}'
                AND product_id IN  (
                        SELECT DISTINCT root_product_id
                        FROM events.root_product
                        WHERE root_product_id IS NOT NULL
                        )
            GROUP BY product_id, date
            """

        queries['set_views'] = """
            TRUNCATE TABLE {schema}.{target_table};
            INSERT INTO {schema}.{target_table}
            SELECT
                products.set_id AS set_id,
                COUNT( DISTINCT views.customer_id ) AS viewers,
                COALESCE(SUM(times_seen),0) AS views
            FROM {schema}.root_product as products
            LEFT JOIN {schema}.products_visited_by_user AS views
                ON products.root_product_id = views.product_id
            WHERE root_product_id IS NOT NULL
                and products.set_id > 0
            GROUP BY set_id;
        """
        return queries

    def insert_root_product(self):
        """
        TODO Warning this query is not OK. It groups by product_id
        but there are more than 1 set per prod id!
        :return:
        """
        sql = """
        SELECT
            p.entity_id
            , p.root_product_id
            , CAST(IFNULL(set_id.category_id, 0) as UNSIGNED) as set_id
        FROM (
            SELECT
                product.entity_id
                , CASE
                    WHEN relation.child_id IS NULL THEN product.entity_id
                    ELSE relation.parent_id END
                AS root_product_id
            FROM monoqib2c.catalog_product_entity product
                LEFT JOIN monoqib2c.catalog_product_relation relation
                    ON relation.child_id = product.entity_id
            GROUP BY entity_id
            ) p
        LEFT JOIN monoqib2c.catalog_category_product set_id
        ON set_id.product_id = p.root_product_id
        group by 1
        ;"""
        df = pd.read_sql(sql=sql
                         , con=self.src_db
                         , index_col='entity_id')
        load_pg_copy(df=df
                     , eng=self.target_db
                     , schema=self.schema
                     , table=self.root_product
                     , truncate=True)

    def load(self):
        _t0_ = time.time()
        self.insert_root_product()
        _t0_ = time.time() - _t0_
        self.log.info(' Update root_products \t time: ' + str(round(_t0_, 2)) + 'secs.')
        _day_ = self.day
        queries = UpdateProductViews.get_queries()
        for tab in self.target_tables:
            _t0_ = time.time()
            _sql = queries[tab].format(day=_day_
                                       , schema=self.schema
                                       , target_table=tab
                                       , src_table=self.src_table)
            self.log.debug(_sql)
            self.target_db.execute(_sql)
            _t0_ = time.time() - _t0_
            self.log.info(tab + '\t time:  ' + str(round(_t0_, 2)) + 'secs.')


if __name__ == "__main__":
    _kwarg_ = parse_args()
    _kwarg_['log_level'] = 'info'
    _kwarg_['day'] = date(2016, 1, 29)
    etl = UpdateProductViews(**_kwarg_)
    # etl.print_conf()
    # etl.create(etl.db)
    etl.run()
    # etl.setup()


