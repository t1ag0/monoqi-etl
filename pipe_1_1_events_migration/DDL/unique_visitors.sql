DROP TABLE IF EXISTS events.unique_visitors;
DROP TABLE IF EXISTS events.unique_visitors_day;
DROP TABLE IF EXISTS events.unique_visitors_week;
DROP TABLE IF EXISTS events.unique_visitors_month;
DROP TABLE IF EXISTS events.unique_visitors_total;

CREATE TABLE events.unique_visitors (
    customer_id INT NOT NULL,
    date DATE NOT NULL,
    hour INT(2) NOT NULL
    PRIMARY KEY (product_id, date, hour)
);

CREATE TABLE events.unique_visitors_day (
    date DATE NOT NULL,
    unique_visitors INT, 
    PRIMARY KEY (date)
);


CREATE TABLE events.unique_visitors_week (
    date DATE NOT NULL,
    unique_visitors INT, 
    PRIMARY KEY (date)
);

CREATE TABLE events.unique_visitors_month (
    date DATE NOT NULL,
    unique_visitors INT, 
    PRIMARY KEY (date)
);

CREATE TABLE events.unique_visitors_total (
  date DATE NOT NULL,
  unique_visitors_day INT,
  unique_visitors_week INT,
  unique_visitors_month INT,
  PRIMARY KEY (date)
);


