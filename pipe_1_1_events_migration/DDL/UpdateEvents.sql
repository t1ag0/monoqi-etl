DROP TABLE IF EXISTS events.events;
CREATE TABLE events.events (
    entity_id BIGINT NOT NULL
    , product_id INT not null
    , visitor_id INT NOT NULL
    , event_type_id SMALLINT NOT NULL
    , event_date DATE
    , event_hour smallint
    , store_id smallint
    , time_of_visit timestamp
    , PRIMARY KEY (entity_id)
)

CREATE INDEX ON events.events USING btree (product_id);
CREATE INDEX ON events.events    USING btree (visitor_id);
CREATE INDEX ON events.events USING btree (event_date, event_type_id);
CREATE INDEX ON events.events USING btree (event_type_id);