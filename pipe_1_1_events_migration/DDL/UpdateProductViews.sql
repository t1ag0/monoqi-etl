DROP TABLE IF EXISTS events.root_product;
DROP TABLE IF EXISTS events.products_visited_by_user;
DROP TABLE IF EXISTS events.product_views;
DROP TABLE IF EXISTS events.product_day_views;
DROP TABLE IF EXISTS events.set_views;

create table events.root_product (
	entity_id INT NOT NULL
	, root_product_id INT NOT NULL
	, set_id INT
	, PRIMARY KEY (entity_id)
	);
CREATE INDEX ON events.root_product USING btree (root_product_id);
CREATE INDEX ON events.root_product USING btree (set_id);

CREATE TABLE events.products_visited_by_user (
  customer_id INT NOT NULL,
  product_id INT NOT NULL,
  date DATE NOT NULL,
  times_seen INT NOT NULL,
  PRIMARY KEY (customer_id, product_id, date)
);
CREATE INDEX ON events.products_visited_by_user USING btree (product_id);
CREATE INDEX ON events.products_visited_by_user USING btree (date);

CREATE TABLE events.product_views (
  root_product_id INT,
  viewers INT NOT NULL,
  views INT NOT NULL,
  PRIMARY KEY (root_product_id)
);

CREATE TABLE events.product_day_views (
  root_product_id INT NOT NULL,
  day DATE NOT NULL,
  viewers INT NOT NULL,
  views INT NOT NULL,
  PRIMARY KEY (root_product_id, day)
);
CREATE INDEX ON events.product_day_views USING btree (day);

CREATE TABLE events.set_views (
  set_id INT,
  viewers INT NOT NULL,
  views INT NOT NULL,
  PRIMARY KEY(set_id)
);

