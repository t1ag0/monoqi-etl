DROP TABLE IF EXISTS warehouse.shop_views_by_day;
CREATE TABLE warehouse.shop_views_by_day (
  date DATE NOT NULL, INDEX(date),
  shop VARCHAR(255) NOT NULL, INDEX(shop),
  views INT(10) NOT NULL, INDEX(views),
  INDEX(shop, date)
) ENGINE=InnoDB DEFAULT CHARSET=utf8

SELECT
  views.date_of_visit as date,
  views.store_id as shop,
  SUM(views.visits) AS views
FROM warehouse.report_event_aggregated AS views
  LEFT JOIN warehouse.products ON(products.id = views.product_id)
WHERE views.event_type_id = 1
GROUP BY views.date_of_visit, views.store_id;
