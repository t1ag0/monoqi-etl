DROP TABLE IF EXISTS events.unique_visitors_by_product_day;
DROP TABLE IF EXISTS events.unique_visitors_by_product_week;
DROP TABLE IF EXISTS events.unique_visitors_by_product_month;
DROP TABLE IF EXISTS events.unique_visitors_by_product_total;
DROP TABLE IF EXISTS events.unique_visitors_by_product;

CREATE TABLE events.unique_visitors_by_product_day
(
	product_id BIGINT NOT NULL,
	date DATE NOT NULL,
	unique_visitors INT NOT NULL DEFAULT 0,
	PRIMARY KEY (product_id, date)
);
CREATE INDEX ON events.unique_visitors_by_product_day  USING btree (date);

CREATE TABLE events.unique_visitors_by_product_week
(
	product_id BIGINT NOT NULL,
	date DATE NOT NULL,
	unique_visitors INT NOT NULL DEFAULT 0,
	PRIMARY KEY (product_id, date)
);
CREATE INDEX ON events.unique_visitors_by_product_week  USING btree (date);

CREATE TABLE events.unique_visitors_by_product_month
(
	product_id BIGINT NOT NULL,
	date DATE NOT NULL,
	unique_visitors INT NOT NULL DEFAULT 0,
	PRIMARY KEY (product_id, date)
);
CREATE INDEX ON events.unique_visitors_by_product_month  USING btree (date);

CREATE TABLE events.unique_visitors_by_product_total
(
	product_id BIGINT NOT NULL,
	unique_visitors INT NOT NULL DEFAULT 0,
	PRIMARY KEY (product_id)
);


DROP TABLE IF EXISTS events.unique_visitors_by_product;
CREATE TABLE events.unique_visitors_by_product (
  product_id BIGINT NOT NULL,
  date DATE NOT NULL,
  unique_visitors_day INT,
  unique_visitors_week INT,
  unique_visitors_month INT,
  unique_visitors_total INT,
  PRIMARY KEY (product_id, date)
);
CREATE INDEX ON events.unique_visitors_by_product  USING btree (date);
