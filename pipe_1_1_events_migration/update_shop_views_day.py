#!/usr/bin/env python3
"""
Update shop views by day
"""
from datetime import date
from sqlalchemy import create_engine
import pandas as pd

from monoqi_etl import MonoqiETL, parse_args, timeit

__author__ = 'Tiago Perez <tiago@cavalcanti.name>'

CLASS_NAME = 'UpdateShopViewsDay'


class UpdateShopViewsDay(MonoqiETL):
    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        #
        self.target_db = create_engine(self.conf['target_db'])
        self.schema = self.conf['schema']
        #
        self.data = None
        self.src_table = self.conf['src_table']
        self.target_table = self.conf['target_table']
        #
        self.day = self.conf['day']

    def create_table(self):
        self.log.info(' CREATE TABLE: ')
        sql = """
            DROP TABLE IF EXISTS {schema}.{table};
            CREATE TABLE {schema}.{table} (
              date DATE NOT NULL,
              shop VARCHAR(255) NOT NULL,
              views INT NOT NULL,
              PRIMARY KEY(shop, date)  
            );
            CREATE INDEX date_shop ON {schema}.{table} 
                USING btree (date,shop);
            """.format(schema=self.schema, table=self.target_table)
        self.target_db.connect()
        self.target_db.execute(sql)

    def load(self):
        #
        # Current AWS PostgreSQL is 9.4, need to delete before insert
        # After PSQL v9.5 support "ON CONFLICT UPDATE"
        sql = """
        DELETE FROM {schema}.{target_table} t where t.date>='{day}';
        INSERT INTO {schema}.{target_table}
        SELECT
            v.event_date as date,
            v.store_id as shop,
            count(v.event_id) AS views
        FROM {schema}.{src_table} AS v
        WHERE v.event_type_id = 1
            AND v.event_date>='{day}'
        GROUP BY
                v.event_date
                , v.store_id;
        """.format(schema=self.schema
                   , target_table=self.target_table
                   , src_table=self.src_table
                   , day=self.day)
        self.target_db.execute(sql)

    def test(self):
        src_sql = """
            select
                v.date
                , v.shop
                , v.views
            from warehouse.shop_views_by_day v
            where v.date='{day}'
            order by 1 asc, 2 asc;""".format(day=self.day)
        target_sql = """
            select
                v.date
                , v.shop
                , v.views
            from {schema}.{table} v
            where v.date='{day}'
            order by 1 asc, 2 asc;""".format(day=self.day
                                             , schema=self.schema
                                             , table=self.target_table)
        src_db = create_engine(self.conf['src_db'])
        src = pd.read_sql(src_sql, src_db
                          , index_col=['date', 'shop'])
        target = pd.read_sql(target_sql, self.target_db
                             , index_col=['date', 'shop'])
        eq = target.equals(src)
        msg = ' Test ' + str(self.day)
        msg += '\n' + str(target.head())
        msg += ' \n\n EQUAL ?  ' + str(eq) + '\n\n'
        self.log.info(msg)
        return eq


# if __name__ == "__main__":
#     """
#     Basic run, updates 1 day.
#     """
#     # _kwarg_ = parse_args()
#     # _kwarg_['day'] = date(2015, 6, 1)
#     # etl = UpdateShopViewsDay(**_kwarg_)
#     # etl.print_conf()
#     # etl.test()
