#!/usr/bin/env python3
"""
Update product views
"""
from datetime import date
from sqlalchemy import create_engine
import time

from monoqi_etl import MonoqiETL, parse_args, timeit

__author__ = 'Tiago Perez <tiago@cavalcanti.name>'

CLASS_NAME = 'UpdateVisitorsProduct'


class UpdateVisitorsProduct(MonoqiETL):
    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        #
        self.db = create_engine(self.conf['db']
                                , pool_reset_on_return='commit')
        self.schema = self.conf['schema']
        #
        # self.data = None
        self.src_table = self.conf['src_table']
        self.target_tables = self.conf['target_tables']
        #
        self.day = self.conf['day']
        # timedelta(days=1)
        # self.day = date(2015, 2, 1)

    @staticmethod
    def get_queries():
        queries = list()
        #
        # DAILY
        # 25sec
        # Unique visitors by product day
        queries.append("""
            DELETE FROM {schema}.{target_table} t where t.date>='{day}';
            INSERT INTO {schema}.{target_table}
            SELECT
                product_id,
                event_date as date,
                count(DISTINCT visitor_id) AS unique_visitors
            FROM events.events e
            WHERE event_type_id = 1
                and e.event_date >= '{day}'
            GROUP BY product_id, date;
        """)

        #
        # WEEKLY
        # 6m 30s
        # 1 week, 7s
        queries.append("""
        DELETE FROM {schema}.{target_table} t
            where t.date>=date_trunc('week',date('{day}'));
        INSERT INTO {schema}.{target_table}
        SELECT
            product_id
            , date_trunc('week', event_date)::date as date
            , count(DISTINCT visitor_id) AS unique_visitors
        FROM events.events e
        WHERE event_type_id = 1
            and e.event_date >= date_trunc('week',date('{day}'))
        GROUP BY 1, 2;
        """)

        # query_visitors_product_month =
        queries.append("""
        DELETE FROM {schema}.{target_table} t where t.date>=date_trunc('month',date('{day}'));
        INSERT INTO {schema}.{target_table}
        SELECT
            product_id
            , date_trunc('month', e.event_date)::date as date
            , count(DISTINCT visitor_id) AS unique_visitors
        FROM events.events e
        WHERE event_type_id = 1
            and e.event_date >= date_trunc('month',date('{day}'))
        GROUP BY product_id, date;
        """)

        # 6m 21s insert / select
        # query_visitors_product_total =
        queries.append("""
        TRUNCATE TABLE {schema}.{target_table};
        INSERT INTO {schema}.{target_table}
        SELECT
          product_id,
          count(DISTINCT visitor_id) AS unique_visitors
        FROM events.events e
        WHERE event_type_id = 1
        GROUP BY product_id;
        """)

        # 3 sec
        queries.append("""
        TRUNCATE TABLE {schema}.{target_table};
        INSERT INTO {schema}.{target_table}
        SELECT daily_visits.product_id,
          daily_visits.date,
          daily_visits.unique_visitors AS unique_visitors_day,
          weekly_visits.unique_visitors AS unique_visitors_week,
          monthly_visits.unique_visitors AS unique_visitors_month,
          total_visits.unique_visitors AS unique_visitors_total
        FROM events.unique_visitors_by_product_day daily_visits
        LEFT JOIN events.unique_visitors_by_product_week weekly_visits
            ON weekly_visits.date = date_trunc('week', daily_visits.date)
            AND weekly_visits.product_id = daily_visits.product_id
        LEFT JOIN events.unique_visitors_by_product_month monthly_visits
            ON monthly_visits.date = date_trunc('month', daily_visits.date)
            AND monthly_visits.product_id = daily_visits.product_id
        LEFT JOIN events.unique_visitors_by_product_total total_visits
            ON total_visits.product_id = daily_visits.product_id
        ;
        """)

        return queries

    def load(self):
        _day_ = self.day
        queries = UpdateVisitorsProduct.get_queries()
        for q, tab in zip(queries, self.target_tables):
            _t0_ = time.time()
            _sql = q.format(day=_day_
                            , target_table=tab
                            , schema=self.schema)
            self.log.debug(_sql)
            self.db.execute(_sql)
            _t1_ = time.time() - _t0_
            self.log.info(" Query " + tab + " time: " + str(round(_t1_, 4)) + " sec.")

if __name__ == "__main__":
    """
    Basic run, updates 1 day.
    """
    _kwarg_ = parse_args()
    _kwarg_['log_level'] = 'debug'
    _kwarg_['day'] = date(2015, 4, 1)
    etl = UpdateVisitorsProduct(**_kwarg_)
    etl.print_conf()
    etl.run()


