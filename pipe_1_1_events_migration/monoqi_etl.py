"""
Base class for ETL classes.
"""
import argparse
import csv
import logging
import os
import pprint
import sys
import time
import yaml
from io import StringIO
from datetime import date, datetime, timedelta

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

__author__ = 'Tiago <tiago@monoqi.de>'
CLASS_NAME = 'MonoqiETL'


def setup_logging(log_name, log_level):
    log = logging.getLogger(log_name)
    log_format = "%(asctime)s - (%(levelname)s) - %(name)s - %(message)s"
    logging.basicConfig(level=log_level,
                        format=log_format,
                        datefmt='%Y-%m-%d %H:%M:%S')
    logging.StreamHandler(sys.__stdout__)
    logging.getLogger().setLevel(log_level.upper())
    return log


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        logging.info('\t\t\t >> Time: ' + str(round(te - ts, 2)) + ' seconds.')
        return result
    return timed


def parse_args():
    parser = argparse.ArgumentParser(description=CLASS_NAME)
    parser.add_argument('--loglevel', '-l', dest='loglevel', action='store',
                        type=str, choices=['debug', 'info', 'warning',
                                           'error', 'critical'],
                        default='INFO', help='log level')

    parser.add_argument('--config', '-c'
                        , dest='config_file'
                        , action='store',
                        type=str,
                        default='update_events.yml',
                        help='Path to configuration file')

    parser.add_argument('--day', '-d', dest='day'
                        , action='store',
                        type=str,
                        help='The date to process',
                        default=str(date.today() - timedelta(days=1))
                        )

    parser.add_argument('--last_day', '-d2', dest='last_day'
                        , action='store',
                        type=str,
                        help='The date to process',
                        default=str(date.today())
                        )

    args = parser.parse_args()
    _log_level_ = args.loglevel.upper()
    _day_ = datetime.strptime(args.day, '%Y-%m-%d').date()
    _day_2 = datetime.strptime(args.last_day, '%Y-%m-%d').date()
    _config_ = args.config_file
    arg_dict = {'log_level': _log_level_
                , 'day': _day_
                , 'last_day': _day_2
                , 'config': _config_}
    return arg_dict


def load_pg_copy(df, eng, schema, table, truncate=False):
    """
    Load a DataFrame into a table using PG Copy.
    :param df: The DataFrame
    :param eng: The SqlAlchemy engine
    :param schema: Target schema
    :param table: Target Table
    :param truncate: If True, truncate table before copy
    :return: None
    """
    csv_buff = StringIO()
    df.to_csv(csv_buff, quoting=csv.QUOTE_NONNUMERIC)
    csv_buff.seek(0)
    #
    Session = sessionmaker(bind=eng)
    session = Session()

    if truncate:
        trunc_sql = """TRUNCATE TABLE {schema}.{table};"""
        trunc_sql = trunc_sql.format(schema=schema, table=table)
        eng.execute(trunc_sql)

    cursor = session.connection().connection.cursor()
    copy_sql = """COPY {schema}.{table} FROM STDIN WITH CSV HEADER;"""
    copy_sql = copy_sql.format(schema=schema, table=table)
    cursor.copy_expert(copy_sql, csv_buff)
    session.commit()
    cursor.close()


class MonoqiETL:
    def __init__(self, **kwargs):
        self.class_name = self.__class__.__name__
        self.log = setup_logging(self.__class__.__name__,
                                 kwargs['log_level'].upper())
        _day = kwargs['day']
        _config = kwargs['config']
        #
        # read config
        import os
        import sys
        if os.path.isfile(_config):
            _config_ = _config
        elif os.path.isfile('../' + _config):
            _config_ = '../' + _config
        else:
            self.log.error(' Config file not found')
            sys.exit(1)
        self.conf = yaml.load(open(_config_, "r"))
        if self.class_name in self.conf:
            self.conf = self.conf[self.class_name]
        else:
            err_msg = 'Config doc: ' + self.class_name + ' not present in file ' + _config
            self.log.error(err_msg)
            raise ReferenceError(err_msg)
        self.day = _day
        #
        self.conf['day'] = _day

    def print_conf(self):
        msg = ' Configuration: \n'
        msg += pprint.pformat(self.conf, indent=4)
        self.log.info(msg)

    def create(self, eng):
        self.log.info(' CREATE ')
        fname = os.path.dirname(__file__)
        fname += '/DDL/' + self.class_name + '.sql'
        with open(fname) as sql:
            sql = sql.read()
            eng.execute(sql)

    def setup(self):
        dbs = ['src_db', 'target_db', 'db']
        for db in dbs:
            if db not in self.conf:
                continue
            try:
                self.log.debug(' Testing ' + db)
                eng = create_engine(self.conf[db])
                eng.connect()
                self.log.debug(' OK ')
            except Exception as e:
                err_msg = """ Can't connect to """ + db
                err_msg += "\n" + str(e)
                self.log.error(err_msg)

    def extract(self):
        pass

    def transform(self):
        pass

    def load(self):
        pass

    def test(self):
        pass

    @timeit
    def run(self):
        self.log.info(" Update from date : " + str(self.day))
        # self.setup()
        self.extract()
        self.transform()
        return self.load()
