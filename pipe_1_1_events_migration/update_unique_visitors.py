#!/usr/bin/env python3
"""
Update product views
"""
from datetime import timedelta, date
from sqlalchemy import create_engine
import time

from monoqi_etl import MonoqiETL, parse_args

__author__ = 'Tiago Perez <tiago@cavalcanti.name>'

CLASS_NAME = 'UpdateUniqueVisitors'


class UpdateUniqueVisitors(MonoqiETL):
    def __init__(self, **kwargs):
        super(self.__class__, self).__init__(**kwargs)
        #
        self.db = create_engine(self.conf['db']
                                , pool_reset_on_return='commit')
        self.schema = self.conf['schema']
        #
        self.src_table = self.conf['src_table']
        self.target_tables = self.conf['target_tables']
        #
        self.day = self.conf['day']

    @staticmethod
    def get_queries():
        queries = dict()
        queries['unique_visitors'] = """
        DELETE FROM {schema}.{target_table} t
            WHERE t.date>='{day}';
        INSERT INTO {schema}.{target_table}
        SELECT
          visitor_id AS customer_id,
          event_date AS date,
          event_hour AS hour
        FROM {schema}.{src_table} e
        WHERE event_type_id = 1
            and event_date >= '{day}'
        GROUP BY visitor_id, event_date, event_hour;
        """
        queries['unique_visitors_day'] = """
        DELETE FROM {schema}.{target_table} t
            WHERE t.date>='{day}';
        INSERT INTO {schema}.{target_table}
        SELECT
          event_date AS date,
          count(DISTINCT visitor_id) AS unique_visitors
        FROM {schema}.{src_table} e
        WHERE event_type_id = 1
            and event_date >= '{day}'
        GROUP BY event_date;
        """

        queries['unique_visitors_week'] = """
        DELETE FROM {schema}.{target_table} t
            WHERE t.date>=date_trunc('week',date('{day}'));
        INSERT INTO {schema}.{target_table}
        SELECT
            date_trunc('week', event_date)::date as date
            , count(DISTINCT visitor_id) AS unique_visitors
        FROM events.events e
        WHERE event_type_id = 1
            and event_date >= date_trunc('week',date('{day}'))
        GROUP BY date;
        """

        queries['unique_visitors_month'] = """
        DELETE FROM {schema}.{target_table} t
            WHERE t.date>=date_trunc('month',date('{day}'));
        INSERT INTO {schema}.{target_table}
        SELECT
            date_trunc('month', event_date)::date as date
            , count(DISTINCT visitor_id) AS unique_visitors
        FROM events.events e
        WHERE event_type_id = 1
            and event_date >= date_trunc('month',date('{day}'))
        GROUP BY date;
        """

        queries['unique_visitors_total'] = """
        TRUNCATE TABLE {schema}.{target_table};
        INSERT INTO {schema}.{target_table}
        SELECT
            daily_visits.date,
            daily_visits.unique_visitors AS unique_visitors_day,
            weekly_visits.unique_visitors AS unique_visitors_week,
            monthly_visits.unique_visitors AS unique_visitors_month
        FROM events.unique_visitors_day AS daily_visits
            LEFT JOIN events.unique_visitors_week AS weekly_visits
                ON weekly_visits.date = date_trunc('week',daily_visits.date)::date
            LEFT JOIN events.unique_visitors_month AS monthly_visits
                ON monthly_visits.date = date_trunc('month', daily_visits.date)::date
        ;"""

        return queries

    def load(self):
        _day_ = self.day
        queries = UpdateUniqueVisitors.get_queries()
        for tab in self.target_tables[:-1]:
            _t0_ = time.clock()
            _sql = queries[tab].format(day=_day_
                                       , target_table=tab
                                       , src_table=self.src_table
                                       , schema=self.schema)
            self.log.debug(_sql)
            self.db.execute(_sql)
            _t1_ = time.clock() - _t0_
            self.log.info(" Query " + tab + " time: " + str(round(_t1_, 4)) + " sec.")

if __name__ == "__main__":
    """
    Basic run, updates 1 day.
    """
    _kwarg_ = parse_args()
    _kwarg_['log_level'] = 'debug'
    # _kwarg_['day'] = date(2015, 4, 1)
    etl = UpdateUniqueVisitors(**_kwarg_)
    etl.print_conf()
    etl.run()

